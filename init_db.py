#!/usr/bin/env python3


# initialize django
import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'CHF_DMP.settings'
import django

django.setup()
import mainpage.models as hmod

from django.db import connection
import subprocess
# ########### DROP DATABASE, RECREATE IT, THEN MIGRATE IT ################ #

cursor = connection.cursor()
cursor.execute("DROP SCHEMA PUBLIC CASCADE")
cursor.execute("CREATE SCHEMA PUBLIC")
subprocess.call([sys.executable, "manage.py", "migrate"])

# drop the tables


# create users
u = hmod.User()
u.first_name = "Zach"
u.last_name = "Trayner"
u.set_password("admin")
u.username = "admin"
u.is_superuser = True
u.save()

u = hmod.User()
u.first_name = "Michael"
u.last_name = "Tyson"
u.set_password("sony")
u.username = "sony"
u.is_superuser = True
u.save()

u = hmod.User()
u.first_name = "Jackie"
u.last_name = "Chan"
u.set_password("jackie")
u.username = "jackie"
u.is_superuser = False
u.save()

u = hmod.User()
u.first_name = "test"
u.last_name = "test"
u.set_password("test")
u.username = "test"
u.is_superuser = False
u.save()

# # create photos
# for data in [
# ["2015-04-11", "Sierra Park", "Bacon", "1"],
# ["2015-06-29", "Timpanogos", "Chicken", "1"],
# ]:
# p = hmod.Photograph()
# p.dateTaken = data[0]
#     p.placeTaken = data[1]
#     p.image = data[2]
#     p.photographer_id = data[3]
#     p.save()

# # create photographable thing
# for data in [
#     ["1"],
#     ["2"],
# ]:
#     p = hmod.PhotographableThing()
#     p.Photo_id = data[0]
#     p.save()

# create rentals
for data in [
    ["2015-02-11", "2015-03-11", "50", "2015-03-25", "10.97", "3", "1"],
    ["2015-02-26", "2015-03-11", "70", "2015-03-27", "280.99", "2", "1"],
    ["2015-03-19", "2015-03-30", "0", None, None, "3", "1"],
]:
    rent = hmod.Rental()
    rent.rentalTime = data[0]
    rent.dueDate = data[1]
    rent.discountPercent = data[2]
    rent.returnTime = data[3]
    # rent.returned = data[4]
    rent.feesPaid = data[4]
    # rent.newDamage = data[5]
    # rent.damageFee = data[6]
    # rent.lateFee = data[7]
    rent.customer_id = data[5]
    rent.agent_id = data[6]
    rent.save()


# create wardrobe items - if rentable add item details in rentable items and put foreign key to one of these
for data in [
    ["Colonial Breeches", "Pants from New England", "39.99", "9.99", "colonialbreeches.jpg", "34", "L",
     "M", "Brown", "None", "1830", "1850", "", None],
    ["Colonial Gloves", "Gloves from New England", "19.99", "3.99", "colonialgloves.jpg", "L", "L", "M",
     "Black", "None", "1830", "1850", "", None],
    ["Colonial Hat", "Hat from New England", "19.99", "3.99", "colonialhat.jpg", "22", "L", "M", "Brown",
     "None", "1830", "1880", "", None],
]:
    wri = hmod.WardrobeItem()
    wri.name = data[0]
    wri.description = data[1]
    wri.value = data[2]
    wri.standardPrice = data[3]
    wri.pictureFileName = data[4]
    wri.size = data[5]
    wri.sizeMod = data[6]
    wri.gender = data[7]
    wri.color = data[8]
    wri.pattern = data[9]
    wri.startYear = data[10]
    wri.endYear = data[11]
    wri.note = data[12]
    wri.rentableWardrobeItem_id = data[13]
    wri.save()

wri1 = hmod.Item.objects.get(id=1)
wri1.rentableWardrobeItem_id = "1"
wri1.save()

wri2 = hmod.Item.objects.get(id=2)
wri2.rentableWardrobeItem_id = "2"
wri2.save()

wri3 = hmod.Item.objects.get(id=3)
wri3.rentableWardrobeItem_id = "3"
wri3.save()


# create rentable items
for data in [
    ["Colonial Boat", "A large boat from New England", "1000.00", "99.99", "New", True, "colonialboat.png"],
    ["Colonial Musket", "Musket from New England", "200.99", "49.99", "Average", True, "colonialmusket.jpg"],
    ["Small Colonial Storefront", "A replica storefront from New England", "11.99", "5.99", "New", True,
     "colonialstorefront.jpg"],
]:
    ri = hmod.RentableItem()
    ri.name = data[0]
    ri.description = data[1]
    ri.value = data[2]
    ri.standardPrice = data[3]
    ri.condition = data[4]
    ri.available = data[5]
    ri.pictureFileName = data[6]
    ri.save()


# create RentalInstances - link rental with rentable items
rentableItem1 = hmod.RentableItem.objects.get(id=4)
rentableItem2 = hmod.RentableItem.objects.get(id=5)
rental1 = hmod.Rental.objects.get(id=1)

rInstance1 = hmod.RentalInstance(Rental=rental1,
                                 RentableItem=rentableItem1, damageFee="100.00",
                                 lateFee="0.00", newDamage="Water damage")
rInstance1.save()

rental2 = hmod.Rental.objects.get(id=2)

rInstance2 = hmod.RentalInstance(Rental=rental2,
                                 RentableItem=rentableItem1, damageFee="0.00",
                                 lateFee="0.00", newDamage="No new damage")
rInstance2.save()

# create items
# for data in [
#     ["knife", "big", "20", "10.97", "knife.jpg"],
#     ["saddle", "brown, long coat", "250", "100", "saddle.jpg"],
#     ["coat", "brown, long coat", "50", "25", "coat.jpg"],
#     ["skit", "full length", "75", "60", "skit.jpg"],
# ]:
#     it = hmod.Item()
#     it.name = data[0]
#     it.description = data[1]
#     it.value = data[2]
#     it.standardPrice = data[3]
#     it.pictureFileName = data[4]
#
#     it.save()

# # create Wardrobe items
# for data in [
#     ["Gove's gloves", "Some sweet colonial era gloves", "10.99", "13.99", "34", "L", "M", "Brown", "Pinstripe", "1830", "1850", "Govesbreeches.jpg", None],
#     ["Colonial Breeches", "Pants from New England", "11.99", "15.99", "L", "36", "M", "Black", "None", "1830", "1880", "colonialbreeches.jpg", "3"],
# ]:
#     r = hmod.WardrobeItem()
#     r.name = data[0]
#     r.description = data[1]
#     r.value = data[2]
#     r.standardPrice = data[3]
#     r.size = data[4]
#     r.sizeMod = data[5]
#     r.gender = data[6]
#     r.color = data[7]
#     r.pattern = data[8]
#     r.startYear = data[9]
#     r.endYear = data[10]
#     r.pictureFileName = data[11]
#     r.rentableItem = data[12]
#     r.save()


# create Event
for data in [
    ["2016-03-28", "2016-03-31", "Kiwana Park Festival", "Festival for all ages", "party_event.jpg", "301 S 300 E", "",
     "Provo", "UT", "84606"],
    ["2016-04-06", "2016-04-09", "Center Colonial Collaboration", "Colonial period festival in downtown Salt Lake City",
     "colonial_event.jpg", "132 Center St.", "", "Provo", "UT", "84606"],
]:
    ev = hmod.Event()
    ev.startDate = data[0]
    ev.endDate = data[1]
    ev.mapName = data[2]
    ev.venueName = data[3]
    ev.pictureFileName = data[4]
    ev.address1 = data[5]
    ev.address2 = data[6]
    ev.city = data[7]
    ev.state = data[8]
    ev.ZIP = data[9]
    ev.save()

# create Areas
for data in [
    ["1", "The area surrounding the model home of George Washington", "1", "George Washington Home", "1", "2",
     "george_washington_home.jpg"],
    ["2", "The area around the Thomas Jefferson Home", "1", "Thomas Jefferson Home", "2",
     "1", "thomas_jefferson_home.jpg"],
    ["1", "The area surrounding the model home of Abraham Lincoln",
     "2", "Abraham Lincoln Log Cabin", "1",
     "2", "abraham_lincoln_cabin.jpg"],
    ["1", "The garden surrounding the mansion of Abraham Lincoln", "2",
     "Abraham Lincoln Gardens", "1", "2", "abraham_lincoln_garden.jpg"],
]:
    a = hmod.Area()
    a.coordinator_id = data[0]
    a.description = data[1]
    a.event_id = data[2]
    a.name = data[3]
    a.placeNum = data[4]
    a.supervisor_id = data[5]
    a.pictureFileName = data[6]
    a.save()

# create SaleItems for areas at events
for data in [
    ["French Revolution Statue", "Super authentic statue", "23.99", "35.99", "1",
     "revolutionstatue.jpg", "29.99", "100.00", "1"],
    ["1st Colonial World Cup", "Cup given to the first victors of the Colonial World Cup",
     "1000.00", "1200.00", "1", "worldcup.jpg", "1099.99", "1500.00", "1"],
    ["1st Colonial Glasses Replica", "A replica is almost as good as an original",
     "400.00", "499.99", "2", "firstglasses.jpg", "399.99", "600.00", "2"],
]:
    si = hmod.SaleItem()
    si.name = data[0]
    si.description = data[1]
    si.value = data[2]
    si.standardPrice = data[3]
    si.person_id = data[4]
    si.pictureFileName = data[5]
    si.lowPrice = data[6]
    si.highPrice = data[7]
    si.saleitem_areas_id = data[8]
    si.save()

# create Product
for data in [
    ["Keychain", "Keychain of a gold eagle", "bulk", "5", "100", "keychain.jpg"],
    ["Cup", "Cup with CHF label", "bulk", "25", "50", "cup.jpg"],
    ["Monticello Model", "A model of the famous Monticello mansion", "bulk", "14.99", "350", "monticellomodel.jpg"],
]:
    r = hmod.Product()
    r.name = data[0]
    r.description = data[1]
    r.category = data[2]
    r.unitPrice = data[3]
    r.qtyOnHand = data[4]
    r.pictureFileName = data[5]
    r.save()