$(function () { 
	
	// bind 'loginform' and provide a simple callback function 
	$('#shopping_cart').ajaxForm(function(data) { 
	    $('#shoppingcart_dialog').find('.modal-body').html(data);
	});

	$('.btn_remove_rental_fromcart').on('click', function() {
 		
 		var pid = $(this).attr('data-pid');
 	
 		$.loadmodal({
 			url: "/mainpage/rentalcart.remove/" + pid,
 			title: 'Shopping Cart',
 			width: '650px',
 		});

 	});//click
});//ready	