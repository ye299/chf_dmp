$(function () { 
	
	// bind 'loginform' and provide a simple callback function 
	$('#shopping_cart').ajaxForm(function(data) { 
	    $('#shoppingcart_dialog').find('.modal-body').html(data);
	});

	$('.btn_removefromcart').on('click', function() {
 		
 		var pid = $(this).attr('data-pid');
 	
 		$.loadmodal({
 			url: "/mainpage/cart.remove/" + pid,
 			title: 'Shopping Cart',
 			width: '650px',
 		});

 	});//click
});//ready	