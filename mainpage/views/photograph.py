from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
import random
import datetime

templater = get_renderer('mainpage')

##Pull photographs from DB
@view_function
def process_request(request):
    params = {}

    params['photographs'] = hmod.Photograph.objects.all()

    return templater.render_to_response(request,'photograph.html',params)


##Edit photographs
@view_function
def edit(request):
    params = {}

    try:
        photograph = hmod.Photograph.objects.get(id=request.urlparams[0])
    except hmod.Photograph.DoesNotExist:
        return HttpResponseRedirect('/mainpage/photograph/')

    form = PhotographEditForm(initial={
        'dateTaken': photograph.dateTaken ,  
        'placeTaken': photograph.placeTaken , 
        'image': photograph.image , 
        
        })

    if request.method == 'POST':
     form = PhotographEditForm(request.POST)
     if form.is_valid():
        photograph.dateTaken = form.cleaned_data['dateTaken']
        photograph.placeTaken = form.cleaned_data['placeTaken']
        photograph.image = form.cleaned_data['image']
        photograph.save()
        return HttpResponseRedirect('/mainpage/photograph/')

    params['form'] = form
    params['photograph'] = photograph

    return templater.render_to_response(request,'photograph.edit.html',params)    

class PhotographEditForm(forms.Form):

    placeTaken = forms.CharField(required=True, max_length=100, label = 'Place Taken', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Place Taken'}))
    dateTaken = forms.DateField(label = 'Date Taken', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Date'}))
    image = forms.CharField(label = 'Image', required=True, max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Image Path'}))


@view_function
def create(request):
    '''Creates a new photograph'''
    photograph = hmod.Photograph()
    photograph.image = ''
    photograph.placeTaken = ''
    photograph.dateTaken = None
    photograph.photographer_id = '3'
    photograph.save()

    return HttpResponseRedirect('/mainpage/photograph.edit/{}/'.format(photograph.id))

@view_function
def delete(request):
    '''Delete an photograph'''
    photograph = hmod.Photograph()
    try:
        photograph = hmod.Photograph.objects.get(id=request.urlparams[0])
    except hmod.Photograph.DoesNotExist:
        return HttpResponseRedirect('/mainpage/photograph/')

    photograph.delete()

    return HttpResponseRedirect('/mainpage/photograph/')    