from django_mako_plus.controller import view_function
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django.http import HttpRequest
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms
from django.conf import settings
from django.contrib.auth.decorators import permission_required
import random
import datetime

templater = get_renderer('mainpage')

##########################################
###### Show a list of Rentals 

@view_function
# @permission_required('mainpage.is_manager', login_url='/mainpage/login/')
def process_request(request):
	params = {}

	rentals = hmod.Rental.objects.all().order_by('dueDate', 'rentalTime')
	params['rentables'] = hmod.RentableItem.objects.all().order_by('name')
	params['rentals'] = rentals

	return templater.render_to_response(request, 'rental.html', params)

###########################################
###### Edit a Rental

@view_function
# @permission_required('mainpage.is_manager', login_url='/mainpage/login/')
def edit(request):
	params = {}

	try:
	  rental = hmod.Rental.objects.get(id=request.urlparams[0])
	except hmod.Rental.DoesNotExist:
		return HttpResponseRedirect('/mainpage/rental/')

	form = rentalEditForm(initial={
		'customer': rental.customer,
		# 'address': rental.address,
		# 'city': rental.city,
		# 'state': rental.state,
		# 'zip': rental.zip,
		# 'email': rental.email,
		# 'phone': rental.phone,		
		'returnTime': rental.returnTime,
		'rentalTime': rental.rentalTime, 
		'dueDate': rental.dueDate,
		'discountPercent': rental.discountPercent,
		'rentalInstance': rental.rentalInstance,
		'agent': rental.agent,
		'feesPaid': rental.feesPaid,
	})

	if request.method == 'POST':
	  form = rentalEditForm(request.POST)
	  form.rentalId = rental.id
	  if form.is_valid():
	   	rental.rentalTime = form.cleaned_data['rentalTime']
	   	rental.dueDate = form.cleaned_data['dueDate']
	   	rental.discountPercent = form.cleaned_data['discountPercent']
	   	rental.rentalInstance = form.cleaned_data['rentalInstance']
	   	rental.agent = form.cleaned_data['agent']
	   	rental.customer = form.cleaned_data['customer']
	   	rental.returnTime = form.cleaned_data['returnTime']
	   	rental.feesPaid = form.cleaned_data['feesPaid']
	   	# rental.address = form.cleaned_data['address']
	   	# rental.city = form.cleaned_data['city']
	   	# rental.state = form.cleaned_data['state']
	   	# rental.zip = form.cleaned_data['zip']
	   	# rental.email = form.cleaned_data['email']
	   	# rental.phone = form.cleaned_data['phone']
	   	# rental.rentedItem = form.cleaned_data['rentedItem']
	   	rental.save()
	   	return HttpResponseRedirect('/mainpage/rental/')

	params['form'] = form
	params['rental'] = rental
	return templater.render_to_response(request, 'rental.edit.html', params)


class rentalEditForm(forms.Form):
	rentalTime = forms.DateField(label="Rental Start Date")
	returnTime = forms.DateField(label="Return Date")
	dueDate = forms.DateField(label="Due Date")
	discountPercent = forms.DecimalField(label="Discount Percent", max_digits=3, decimal_places=2)
	rentalInstance = forms.CharField(label="Rental Price")
	feesPaid = forms.DecimalField(label="Paid Fee")
	# agent =  forms.CharField(label="Agent", max_length=50)
	agent = forms.ModelMultipleChoiceField(queryset=hmod.User.objects.filter(is_agent = True))
	customer = forms.CharField(label="Renter", max_length=50)
	# address = forms.CharField(label="Address", max_length=50)
	# city = forms.CharField(label="City", max_length=50)
	# state = forms.CharField(label="State", max_length=50)
	# zip = forms.CharField(label="Zip", max_length=50)
	# email = forms.CharField(label="Email", max_length=50)
	# phone = forms.CharField(label="Phone", max_length=50)
	# rentedItem = forms.CharField(label="Item Rented", max_length=50)


###########################################
###### Create a Rental

@view_function
# @permission_required('mainpage.is_manager', login_url='/mainpage/login/')
def create(request):

	
	rentable = hmod.RentableItem()
	rentalInstance = hmod.RentalInstance(RentableItem=rentable )
	rental = hmod.Rental(RentalInstance=rentalInstance)

	rentable 
	rental.rentalTime = '2015-01-01'
	rental.returnTime = '2015-01-01'
	rental.dueDate = '2015-01-01'
	rental.discountPercent = .00
	rental.rentalInstance = 5.00
	rental.feesPaid = 1.99
	rental.agent = ''
	rental.customer = ''
	# rental.address = ''
	# rental.city = ''
	# rental.state = ''
	# rental.zip = ''
	# rental.email = ''
	# rental.phone = ''
	# rental.rentedItem = ''
	rental.save()

	return HttpResponseRedirect('/mainpage/rental.edit/{}/'.format(rental.id))


###########################################
###### Delete a Rental

@view_function
# @permission_required('mainpage.is_admin', login_url='/mainpage/login/')
def delete(request):

	try:
	 rental = hmod.Rental.objects.get(id=request.urlparams[0])
	except hmod.Rental.DoesNotExist:
	  return HttpResponseRedirect('/mainpage/rental/')

	rental.delete()

	return HttpResponseRedirect('/mainpage/rental/')

###########################################
###### Overdue Rentals

@view_function
# @permission_required('mainpage.is_manager', login_url='/mainpage/login/')
def overdue(request):

	params = {}
	
	now = datetime.datetime.now()
	items = hmod.RentableItem.objects.all()
	rentals = hmod.RentalInstance.objects.filter(RentableItem = request.urlparams[0])
	rentals = hmod.Rental.objects.filter(dueDate__lte=now).order_by('dueDate', 'rentalTime')
	
	
	params['rentals'] = rentals
	params['items'] = items	

	return templater.render_to_response(request, 'rental.overdue.html', params)


@view_function
def process_return(request):

	params = {}

	try:
		rental = hmod.Rental.objects.get(id=request.urlparams[0])
		# sales = hmod.SaleItem.objects.filter(saleitem_areas_id=request.urlparams[0])
	except hmod.Rental.DoesNotExist:
		return HttpResponseRedirect('/mainpage/rental/')

	rentalInstance = hmod.RentalInstance.objects.filter(id = request.urlparams[0])

	form = RentalReturnForm(initial={
		# 'Rental ID': rental.id,
		'customer': rental.customer.first_name + ' ' + rental.customer.last_name,
		# 'newDamage': rentalInstance.newDamage,	
		'rentalTime': rental.rentalTime, 
		'dueDate': rental.dueDate,
		'discountPercent': rental.discountPercent,
		'feesPaid': rental.feesPaid,
		'agent': rental.agent.first_name + ' ' + rental.agent.last_name,
		'returnTime': rental.returnTime,
	})

	if request.method == 'POST':
	  form = RentalReturnForm(request.POST)
	  # form.rentalId = rental.id
	  if form.is_valid():
	   	rental.rentalTime = form.cleaned_data['rentalTime']
	   	rental.dueDate = form.cleaned_data['dueDate']
	   	# rental.discountPercent = form.cleaned_data['discountPercent']
	   	# rental.agent = form.cleaned_data['agent']
	   	# rental.customer = form.cleaned_data['customer']
	   	rental.returnTime = form.cleaned_data['returnTime']
	   	rental.feesPaid = form.cleaned_data['feesPaid']
	   	rentalInstance.newDamage = form.cleaned_data['newDamage']
	   	rental.save()
	   	# rentalInstance.save()
	   	return HttpResponseRedirect('/mainpage/rental/')

	params['form'] = form
	params['rental'] = rental
	return templater.render_to_response(request, 'rental.process_return.html', params)


class RentalReturnForm(forms.Form):
	customer = forms.CharField(label="Customer", max_length=50, widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))
	rentalTime = forms.DateField(label="Rental Date", widget=forms.DateInput(attrs={'class': 'form-control','readonly':'readonly'}))
	agent = forms.CharField(label="Agent", max_length=50, widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))
	dueDate = forms.DateField(label="Due Date", widget=forms.DateInput(attrs={'class': 'form-control','readonly':'readonly'}))
	discountPercent = forms.DecimalField(label="Discount Percent", widget=forms.TextInput(attrs={'class': 'form-control','readonly':'readonly'}))
	newDamage = forms.CharField(required= False,label="New Damage", max_length=50, widget=forms.Textarea(attrs={'class': 'form-control'}))
	damageFee = forms.DecimalField(required= False,label="Damage Fee", widget=forms.TextInput(attrs={'class': 'form-control'}))
	lateFee = forms.DecimalField(required= False,label="Late Fee", widget=forms.TextInput(attrs={'class': 'form-control'}))
	feesPaid = forms.DecimalField(required= False,label="Paid Fee", widget=forms.TextInput(attrs={'class': 'form-control'}))
	returnTime = forms.DateField(label="Return Date", widget=forms.DateInput(attrs={'class': 'form-control', 'placeholder': 'Enter Return Date'}))
	
	
# @view_function
# def send_email(request):

# 	try:
# 		user = hmod.User.objects.get(id=request.urlparams[0])
# 	except hmod.User.DoesNotExist:
# 		return HttpResponseRedirect('/mainpage/')

# 	subject = 'Thanks! Here is your receipt.'#request.POST.get('subject', '')
# 	message = 'Dear Customer, your order has been submitted.'#request.POST.get('message', '')
# 	from_email = 'yesukhei0822@gmail.com'#request.POST.get('from_email', '')
# 	to_email = [user.email]
# 	if subject and message and from_email:
# 		try:
# 			send_mail(subject, message, from_email, to_email)
# 		except BadHeaderError:
# 			return HttpResponse('Invalid header found.')
# 		return HttpResponseRedirect('/mainpage/thankyou/')

# 	else:
# 		return HttpResponse('/mainpage/thankyou/')