from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
import random
import datetime

templater = get_renderer('mainpage')

##Pull events from DB
@view_function
def process_request(request):
    params = {}

    params['events'] = hmod.Event.objects.all()

    return templater.render_to_response(request,'event.html',params)
  

@view_function
def info(request):
    params = {}

    try:
        event = hmod.Event.objects.get(id=request.urlparams[0])
        area = hmod.Area.objects.filter(event_id=request.urlparams[0])
        sales = hmod.SaleItem.objects.filter(saleitem_areas_id=request.urlparams[0])
    except hmod.Event.DoesNotExist:
        return HttpResponseRedirect('/mainpage/event/')

    params['event'] = event
    params['areas'] = area
    params['sales'] = sales

    return templater.render_to_response(request, 'event.info.html', params)


##Edit events
@view_function
def edit(request):
    params = {}

    try:
        event = hmod.Event.objects.get(id=request.urlparams[0])
    except hmod.Event.DoesNotExist:
        return HttpResponseRedirect('/mainpage/event/')

    form = EventEditForm(initial={
        'startDate': event.startDate ,  
        'endDate': event.endDate , 
        'mapName': event.mapName , 
        
        })

    if request.method == 'POST':
     form = EventEditForm(request.POST)
     if form.is_valid():
        event.mapName = form.cleaned_data['mapName']
        event.startDate = form.cleaned_data['startDate']
        event.endDate = form.cleaned_data['endDate']
        event.venueName = form.cleaned_data['venueName'] 
        event.save()
        return HttpResponseRedirect('/mainpage/event/')

    params['form'] = form
    params['event'] = event

    return templater.render_to_response(request,'event.edit.html',params)    

class EventEditForm(forms.Form):

    mapName = forms.CharField(required=True, max_length=100, label = 'Map Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Map Name'}))
    startDate = forms.DateField(label = 'Start Date', widget=forms.DateInput(attrs={'class': 'form-control', 'placeholder': 'Enter Date'}))
    endDate = forms.DateField(label = 'End Date', widget=forms.DateInput(attrs={'class': 'form-control', 'placeholder': 'Enter Date'}))
    venueName = forms.CharField(required=True, max_length=100, label = 'Venue Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Name'}))


@view_function
def create(request):
    '''Creates a new event'''
    event = hmod.Event()
    event.mapName = ''
    event.venueName = ''
    event.startDate = None
    event.endDate = None
    event.save()

    return HttpResponseRedirect('/mainpage/event.edit/{}/'.format(event.id))

@view_function
def delete(request):
    '''Delete an event'''
    event = hmod.Event()
    try:
        event = hmod.Event.objects.get(id=request.urlparams[0])
    except hmod.Event.DoesNotExist:
        return HttpResponseRedirect('/mainpage/event/')

    event.delete()

    return HttpResponseRedirect('/mainpage/event/')    