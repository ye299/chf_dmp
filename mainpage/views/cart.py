from django_mako_plus.controller import view_function
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django.http import HttpRequest
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms
from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.decorators import permission_required
import random


templater = get_renderer('mainpage')

##########################################
###### Show a list of products

# @permission_required('mainpage.is_user', login_url='/mainpage/login/')
@view_function
def process_request(request):
	params = {}

	##### Products ######
	cartProducts = {}
	cartProducts = request.session['shopping_cart']

	currentCart = {}

	for key, value in cartProducts.items():
		item = hmod.Product.objects.get(id=key)
		productContainer = []
		productContainer.append(item)
		productContainer.append(value)
		currentCart[item.id] = productContainer

	params['currentCart'] = currentCart


	return templater.render_to_response(request, 'cart.html', params)

# @permission_required('mainpage.is_user', login_url='/mainpage/login/')
@view_function
def add(request):
	params = {}

	if 'shopping_cart' not in request.session:
		request.session['shopping_cart'] = {}

	pid = request.urlparams[0]
	qty = int(request.urlparams[1])

	if pid in request.session['shopping_cart']:
	 	request.session['shopping_cart'][pid] += qty
	 	request.session.modified = True
	else: 
		request.session['shopping_cart'][pid] = qty
		request.session.modified = True

	return HttpResponseRedirect('/mainpage/cart/')

# @permission_required('mainpage.is_user', login_url='/mainpage/login/')
@view_function
def remove(request):
	pid = request.urlparams[0]

	del request.session['shopping_cart'][pid]
	request.session.modified = True

	return HttpResponseRedirect('/mainpage/cart/')