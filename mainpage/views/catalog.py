from django_mako_plus.controller import view_function
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django.http import HttpRequest
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django import forms
from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.decorators import permission_required
import random
from django.core.mail import send_mail, BadHeaderError
import requests
import json


templater = get_renderer('mainpage')

##########################################
###### Show a list of products

@view_function
def process_request(request):
	params = {}

	if request.urlparams[0]:
	 products = hmod.Product.objects.filter(name__icontains=request.urlparams[0])
	else:
	 products = hmod.Product.objects.all()
	#individualproducts = hmod.IndividualProduct.objects.all()
	params['products'] = products
	#params['individualproducts'] = individualproducts

	return templater.render_to_response(request, 'catalog.html', params)

@view_function
def detail_product(request):
	params = {}

	try:
	  product = hmod.Product.objects.get(id=request.urlparams[0])
	except hmod.Product.DoesNotExist:
		return HttpResponseRedirect('/mainpage/catalog.html')
	params['product'] = product

	return templater.render_to_response(request, 'catalog.detail_product.html', params)


# # @permission_required('mainpage.is_user', login_url='/mainpage/login/')
# @view_function
# def shopping_cart(request):
	
# 	if 'shopping_cart' not in request.session:
# 		request.session['shopping_cart'] = {}
# 	if item.id not in request.session['shopping_cart']:
# 	 	request.session['shopping_cart'][item.id] = 1
# 	else: 
# 		request.session['shopping_cart'][item.id] += 1

# 	request.session.modified = True

# 	return templater.render_to_response(request, 'catalog.shopping_cart.html', params)

# @permission_required('mainpage.is_user', login_url='/mainpage/login/')
@view_function
def checkout(request):
	# params = {}

	# current_user = request.user
	# user = hmod.User.objects.get(id=current_user.id)
	

	# form = ShippingForm(initial={
	# 	'first_name': user.first_name,
	# 	'last_name': user.last_name,
	# 	# 'address1': user.address,
	# 	# 'city': user.city,
	# 	# 'state': user.state,
	# 	# 'zip': user.Zip,
	# 	'phone': user.phone,
	# 	'email': user.email,
	# 	'creditcard':'',
	# 	'expirationdate':'',
	# 	'securitycode':''
	# })

	# if request.method == 'POST':
	#   form = ShippingForm(request.POST)
	#   if form.is_valid():
	#    	user.firstname = form.cleaned_data['first_name']
	#    	user.last_name = form.cleaned_data['last_name']
	#    	# user.address1 = form.cleaned_data['address1']
	#    	# user.city = form.cleaned_data['city']
	#    	# user.state = form.cleaned_data['state']
	#    	# user.zip = form.cleaned_data['zip']
	#    	user.phone = form.cleaned_data['phone']
	#    	user.email = form.cleaned_data['email']
	#    	creditcard = form.cleaned_data['creditcard']
	#    	expirationdate = form.cleaned_data['expirationdate']
	#    	securitycode = form.cleaned_data['securitycode']

	#    	return HttpResponseRedirect('/mainpage/catalog.send_email/')


	current_user = request.user
	user = hmod.User.objects.get(id=current_user.id)

	params = {}
	form = CheckoutForm(initial={
		'first_name': user.first_name,
		'last_name': user.last_name,
		'amount': request.urlparams[0],
		'name': 'Cosmo Limesandal',
		'type': 'Visa',
		'number': '4732817300654',
		'exp_month': '10',
		'exp_year': '15',
		'cvc': '411',
		'description': '',
	})

	if request.method == 'POST':
		form = CheckoutForm(request.POST)
		if form.is_valid():
			user.firstname = form.cleaned_data['first_name']
			user.last_name = form.cleaned_data['last_name']
			return HttpResponseRedirect('/mainpage/catalog.send_email/')


	API_URL = 'http://dithers.cs.byu.edu/iscore/api/v1/charges'
	API_KEY = 'ad83931bc24b5c44c1879efb7ff4e780'

	r = requests.post(API_URL, data={
		'apiKey': API_KEY,
		'currency': 'usd',
		'amount': 'amount',
		'type': 'type',
		'Number': 'number',
		'exp_month': 'exp_month',
		'exp_year': 'exp_year',
		'cvc': 'cvc',
		'name': 'name',
		'description': 'description'
	})
	print(r.text)
	resp = r.json()
	if 'error' in resp:
		print('ERROR: ', resp['error'])
	else:
		print(resp.keys())
		print(resp['ID'])

	params['form'] = form
	params['user'] = user
	return templater.render_to_response(request, 'catalog.checkout.html', params)

class CheckoutForm(forms.Form):
	first_name=forms.CharField(label="First Name", required=True, max_length=100)
	last_name=forms.CharField(label="Last Name", required=True, max_length=100)
	amount = forms.DecimalField(label='Total', required=True)
	type = forms.CharField(label='Type of Card', required=True)
	number = forms.IntegerField(label='Card Number', required=True)
	exp_month = forms.IntegerField(label='Experiation Month', required=True)
	exp_year = forms.IntegerField(label='Experation Year', required=True)
	cvc = forms.IntegerField(label='CVC', required=True)
	name = forms.CharField(label='Full Name')
	description = forms.CharField(label='Description', required=False)



# class ShippingForm(forms.Form):
#  first_name=forms.CharField(label="First Name", required=True, max_length=100)
#  last_name=forms.CharField(label="Last Name", required=True, max_length=100)
#  # address1=forms.CharField(label="Shipping Address", required=True, max_length=100)
#  # city=forms.CharField(label="City", required=True, max_length=100)
#  # state=forms.CharField(label="State", required=True, max_length=100)
#  # zip=forms.CharField(label="Zip", required=True, max_length=100)
#  phone=forms.CharField(label="Phone Number", required=True, max_length=100)
#  email=forms.EmailField(label="Email", required=True, max_length=100)
#  creditcard=forms.CharField(label="Credit Card Number", required=True, min_length=16, max_length=16)
#  expirationdate=forms.CharField(label="Expiration Date", required=True,min_length=5, max_length=5)
#  securitycode=forms.CharField(label="Security Code", required=True, min_length=3, max_length=4)


@view_function
def send_email(request):

	# try:
	# 	user = hmod.User.objects.get(id=request.urlparams[0])
	# except hmod.User.DoesNotExist:
	# 	return HttpResponseRedirect('/mainpage/')

	subject = 'Thanks! Here is your receipt.'#request.POST.get('subject', '')
	message = 'Dear Customer, your order has been submitted.'#request.POST.get('message', '')
	from_email = 'yesukhei0822@gmail.com'#request.POST.get('from_email', '')
	to_email = ['yesukhei0822@gmail.com']
	if subject and message and from_email:
		try:
			send_mail(subject, message, from_email, to_email)
		except BadHeaderError:
			return HttpResponse('Invalid header found.')
		return HttpResponseRedirect('/mainpage/thankyou/')

	else:
	    return HttpResponse('/mainpage/thankyou/')

