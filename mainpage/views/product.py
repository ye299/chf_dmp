from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
from django.contrib.auth.decorators import permission_required
import random
import datetime

templater = get_renderer('mainpage')

##Pull products from DB
@view_function
def process_request(request):
    params = {}
    params['products'] = hmod.Product.objects.all()
    return templater.render_to_response(request,'product.html',params)


##Edit BulkProducts
@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def edit(request):
    params = {}

    try:
        product = hmod.Product.objects.get(id=request.urlparams[0])
    except hmod.Product.DoesNotExist:
        return HttpResponseRedirect('/mainpage/product/')

    form = ProductEditForm(initial={
        'name': product.name ,
        'description': product.description ,
        'category': product.category ,
        'unitPrice': product.unitPrice ,
        'qtyOnHand': product.qtyOnHand ,
        # 'company': product.company ,
        # 'order': product ,
        })

    if request.method == 'POST':
     form = ProductEditForm(request.POST)
     form.bulkproductid = product.id
     if form.is_valid():

        product.name = form.cleaned_data['name']
        product.description = form.cleaned_data['description']
        product.category = form.cleaned_data['category']
        product.unitPrice = form.cleaned_data['unitPrice']
        product.qtyOnHand = form.cleaned_data['qtyOnHand']
        product.picture = form.cleaned_data['picture']
        product.save()
        return HttpResponseRedirect('/mainpage/product/')

    params['form'] = form
    params['product'] = product

    return templater.render_to_response(request,'product.edit.html',params)    

class ProductEditForm(forms.Form):

    name = forms.CharField(required=True, max_length=100, label = 'Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'product Name'}))
    description = forms.CharField(required=True, max_length=100, label = 'Description', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Description'}))
    category = forms.CharField(required=True, max_length=100, label = 'Category', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Category'}))
    unitPrice = forms.DecimalField(required=True, label = 'Unit Price', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Price'}))
    qtyOnHand = forms.IntegerField(required=True, label = 'Qty', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Qty'}))
    picture = forms.CharField(required=True, max_length=100, label = 'Image', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Img'}))

@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def create(request):
    '''Creates a new product'''
    product = hmod.Product()
    product.name = ''   
    product.description = ''
    product.category = ''
    product.unitPrice = '0.0'
    product.picture = ''
    product.qtyOnHand = '1'
    product.company_id = '3'
    product.order_id = '1'
    product.save()

    return HttpResponseRedirect('/mainpage/product.edit/{}/'.format(product.id))

@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def delete(request):
    '''Delete a product'''
    product = hmod.Product()
    try:
        product = hmod.Product.objects.get(id=request.urlparams[0])
    except hmod.Product.DoesNotExist:
        return HttpResponseRedirect('/mainpage/product/')

    product.delete()

    return HttpResponseRedirect('/mainpage/product/')    


@view_function
def productdetail(request):
    params ={}
    try:
        params['product'] = hmod.Product.objects.get(id=request.urlparams[0])
    except hmod.Product.DoesNotExist:
        return HttpResponseRedirect('/mainpage/product/')

    return templater.render_to_response(request,'productdetail.html',params)  

@view_function
def checkout(request):
    params ={}
    try:
        params['product'] = hmod.Product.objects.get(id=request.urlparams[0])
    except hmod.Product.DoesNotExist:
        return HttpResponseRedirect('/mainpage/product/')

    return templater.render_to_response(request,'checkout.html',params)     
