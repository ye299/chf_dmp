from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django.contrib.auth.decorators import permission_required

from django import forms
import random
import datetime

templater = get_renderer('mainpage')

##Pull Agents from DB
@view_function
# @permission_required('is_stafdf', login_url='/mainpage/login', raise_exception=False)
def process_request(request):
    params = {}

    params['participants'] = hmod.Participant.objects.all()

    return templater.render_to_response(request,'participant.html',params)


##Edit Agents
@view_function
def edit(request):
    params = {}

    try:
        participant = hmod.Participant.objects.get(id=request.urlparams[0])
    except hmod.Participant.DoesNotExist:
        return HttpResponseRedirect('/mainpage/participant/')

    form = ParticipantEditForm(initial={
        'sketch': participant.sketch ,  
        'contactRel': participant.contactRel , 
        'IDphoto': participant.IDphoto , 
        'roleName': participant.roleName ,
        'roleType': participant.roleType ,
        })

    if request.method == 'POST':
     form = ParticipantEditForm(request.POST)
     if form.is_valid():
        participant.sketch = form.cleaned_data['sketch']
        participant.contactRel = form.cleaned_data['contactRel']
        participant.IDphoto = form.cleaned_data['IDphoto']
        participant.roleName = form.cleaned_data['roleName']
        participant.roleType = form.cleaned_data['roleType']
        participant.save()
        return HttpResponseRedirect('/mainpage/participant/')

    params['form'] = form
    params['participant'] = participant

    return templater.render_to_response(request,'participant.edit.html',params)    

class ParticipantEditForm(forms.Form):

    sketch = forms.CharField(required=True, max_length=100, label = 'Sketch', widget=forms.TextInput(attrs={'class': 'form-control'}))
    contactRel = forms.CharField(required=True, max_length=100, label = 'Contact Relationship', widget=forms.TextInput(attrs={'class': 'form-control'}))
    IDphoto = forms.CharField(required=True, max_length=100, label = 'Photo ID', widget=forms.TextInput(attrs={'class': 'form-control'}))    
    roleName = forms.CharField(required=True, max_length=100, label = 'Role Name', widget=forms.TextInput(attrs={'class': 'form-control'}))
    roleType = forms.CharField(required=True, max_length=100, label = 'Role Type', widget=forms.TextInput(attrs={'class': 'form-control'}))


@view_function
def create(request):
    '''Creates a new participant'''
    participant = hmod.Participant()
    participant.sketch = ''
    participant.contactRel = ''
    participant.IDphoto = ''
    participant.roleName = ''
    participant.roleType = ''
    participant.person_id = '3'
    participant.area_id = '3'
    participant.save()

    return HttpResponseRedirect('/mainpage/participant.edit/{}/'.format(participant.id))

@view_function
def delete(request):
    '''Delete an participant'''
    participant = hmod.Participant()
    try:
        participant = hmod.Participant.objects.get(id=request.urlparams[0])
    except hmod.Participant.DoesNotExist:
        return HttpResponseRedirect('/mainpage/participant/')

    participant.delete()

    return HttpResponseRedirect('/mainpage/participant/')    