from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
from django.contrib.auth.decorators import permission_required
import random
import datetime

templater = get_renderer('mainpage')

##Pull users from DB
@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def process_request(request):
    params = {}

    params['users'] = hmod.User.objects.all().order_by('id')
    return templater.render_to_response(request,'user.html',params)


##Edit Users
@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def edit(request):
    params = {}

    try:
        user = hmod.User.objects.get(id=request.urlparams[0])
    except hmod.User.DoesNotExist:
        return HttpResponseRedirect('/mainpage/user/')

    form = UserEditForm(initial={
        'password': user.password ,
        'last_login': user.last_login ,
        'username': user.username ,
        'first_name': user.first_name ,
        'last_name': user.last_name , 
        'email': user.email ,
        'phone': user.phone ,
        'organizationType': user.organizationType ,
        'is_agent': user.is_agent ,
        'is_staff': user.is_staff ,
        'is_active': user.is_active ,
        'date_joined': user.date_joined ,
        # 'appointmentDate': user.appointmentDate ,

        })

    if request.method == 'POST':
     form = UserEditForm(request.POST)
     form.userid = user.id
     if form.is_valid():
        user.set_password(form.cleaned_data['password'])
        user.last_login = form.cleaned_data['last_login']
        user.username = form.cleaned_data['username']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.email = form.cleaned_data['email']
        user.phone = form.cleaned_data['phone']
        user.date_joined = form.cleaned_data['date_joined']
        # user.appointmentDate = form.cleaned_data['appointmentDate']
        user.security_question = form.cleaned_data['security_question']
        user.security_answer = form.cleaned_data['security_answer']
        user.organizationType = form.cleaned_data['organizationType']
        user.save()
        return HttpResponseRedirect('/mainpage/user/')

    params['form'] = form
    params['user'] = user

    return templater.render_to_response(request,'user.edit.html',params)    

class UserEditForm(forms.Form):

    username = forms.CharField(required=True, max_length=100, label = 'User Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'User Name'}))
    first_name = forms.CharField(required=True, max_length=100, label = 'First Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}))
    last_name = forms.CharField(required=True, max_length=100, label = 'Last Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}))
    password = forms.CharField(required=True, max_length=100, label = 'Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    email = forms.CharField(required=True, max_length=100, label = 'Email', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    phone = forms.CharField(required=True, max_length=100, label = 'Phone', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Phone'}))
    security_question = forms.CharField(label = 'Security Question', widget=forms.TextInput(attrs={'class': 'form-control'}))
    security_answer = forms.CharField(label = 'Security Answer', widget=forms.TextInput(attrs={'class': 'form-control'}))
    organizationType = forms.CharField(label = 'Company', widget=forms.TextInput(attrs={'class': 'form-control'}))
    # appointmentDate = forms.DateField(label = 'Appointment Date', widget=forms.DateInput(attrs={'class': 'form-control'}))
    date_joined = forms.DateField(label = 'Date Joined', widget=forms.DateInput(attrs={'class': 'form-control','readonly':'readonly'}))
    last_login = forms.DateField(label = 'Last Login', widget=forms.DateInput(attrs={'class': 'form-control','readonly':'readonly'}))


def clean_username(self):
    user_count = hmod.User.objects.filter(username=self.cleaned_data['username']).exclude(id=self.userid).count()
    if user_count >= 1:
        raise forms.ValidationError("This username is already taken.")
    return self.cleaned_data['username']



@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def create(request):
    '''Creates a new user'''
    user = hmod.User()
    user.username = ''   
    user.first_name = ''
    user.last_name = ''
    user.password = ''
    user.email = ''
    user.phone = ''
    user.organizationType = ''
    user.security_question = ''
    user.security_answer = ''
    # user.appointmentDate = datetime.datetime.now()
    user.last_login = datetime.datetime.now()
    user.date_joined = datetime.datetime.now()
    user.save()

    return HttpResponseRedirect('/mainpage/user.edit/{}/'.format(user.id))

@view_function
# @permission_required('is_superuser', login_url='/mainpage/login', raise_exception=False)
def delete(request):
    '''Delete a user'''
    user = hmod.User()
    try:
        user = hmod.User.objects.get(id=request.urlparams[0])
    except hmod.User.DoesNotExist:
        return HttpResponseRedirect('/mainpage/user/')

    user.delete()

    return HttpResponseRedirect('/mainpage/user/')    