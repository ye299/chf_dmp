from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
import random
import datetime

templater = get_renderer('mainpage')

##Pull Returns from DB
@view_function
def process_request(request):
    params = {}

    params['itemreturns'] = hmod.PublicEvent.objects.all()

    return templater.render_to_response(request,'publicevent.html',params)


##Edit Returns
@view_function
def edit(request):
    params = {}

    try:
        publicevent = hmod.PublicEvent.objects.get(id=request.urlparams[0])
    except hmod.PublicEvent.DoesNotExist:
        return HttpResponseRedirect('/mainpage.publicevent/')

    form = ReturnEditForm(initial={
        'name': publicevent.name ,  
        'description': publicevent.description , 
        })

    if request.method == 'POST':
     form = ReturnEditForm(request.POST)
     if form.is_valid():
        publicevent.name = form.cleaned_data['name']
        publicevent.description = form.cleaned_data['description']
        publicevent.save()
        return HttpResponseRedirect('/mainpage.publicevent/')

    params['form'] = form
    params['publicevent'] = publicevent

    return templater.render_to_response(request,'publicevent.edit.html',params)    

class ReturnEditForm(forms.Form):

    name = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))

@view_function
def create(request):
    '''Creates a new.PublicEvent'''
    publicevent = hmod.PublicEvent()
    publicevent.name = ''
    publicevent.description = ''
    publicevent.agent = '4'
    publicevent.save()

    return HttpResponseRedirect('/mainpage.publicevent.edit/{}/'.format.publicevent.id))

@view_function
def delete(request):
    '''Delete an.PublicEvent'''
    publicevent = hmod.PublicEvent()
    try:
        publicevent = hmod.PublicEvent.objects.get(id=request.urlparams[0])
    except hmod.PublicEvent.DoesNotExist:
        return HttpResponseRedirect('/mainpage.publicevent/')

    publicevent.delete()

    return HttpResponseRedirect('/mainpage.publicevent/')    