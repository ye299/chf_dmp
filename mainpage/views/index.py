from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
from django.contrib.auth import authenticate, login
from django.contrib import auth
import random
import datetime
from ldap3 import Server, Connection, AUTH_SIMPLE, STRATEGY_SYNC, GET_ALL_INFO
import json

templater = get_renderer('mainpage')


##Login
@view_function
def process_request(request):
    params = {}

    return templater.render_to_response(request,'index.html',params)    

class LoginForm(forms.Form):

    username = forms.CharField(required=True, max_length=100, label = "User Name", widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label="Password",widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean(self):
        user = authenticate(username=self.cleaned_data['username'], password=self.cleaned_data['password'])
        if user == None:
            raise forms.ValidationError('Username and password is incorrect.')
        return self.cleaned_data

@view_function
def loginform(request):
    template_vars = {}

    form = LoginForm()
    print(1)
    if request.method == 'POST':
        print(2)
        form = LoginForm(request.POST)

        if form.is_valid():
            # netid = 'ye299'
            # pw = ''
            # s = Server('byuldap.byu.edu', port=389, get_info=GET_ALL_INFO)
            # c = Connection(s, auto_bind=True, client_strategy=STRATEGY_SYNC,
            #             user='%s,ou=people,0=ces' % netid,
            #             password=pw, authentication=AUTH_SIMPLE)
            # if c:
            #     u , created = hmod.User.objects.get_or_create(username=netid)
            #     u.first_name = c.user.first_name
            #     #u.last_name =
            #     #u.email =
            #     u.set_password(pw)
            #     u.save()
            #     u2 = authenticate(username, password)
            #     login(request, u2)

            # else:

            print(3)
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            print(4)
            return HttpResponse('''
                <script>
                console.log('hey hey hye');
                    window.location.href = "/account/myaccount/${user.id}";
                </script>
            ''')

    template_vars['form'] = form
    return templater.render_to_response(request,'index.loginform.html',template_vars) 
    
@view_function
def logout(request):

    auth.logout(request)

    return HttpResponseRedirect('/mainpage/logout/')
