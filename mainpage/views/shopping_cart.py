from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django_mako_plus.controller.router import get_renderer
from django import forms
from django.contrib.auth import authenticate, login
from django.contrib import auth
import random
import datetime

templater = get_renderer('mainpage')

@view_function
def process_request(request):
    params = {}

    cartProducts = {}
    cartProducts = request.session['shopping_cart']

    currentCart = {}

    for key, value in cartProducts.items():
        item = hmod.Product.objects.get(id=key)
        productContainer = []
        productContainer.append(item)
        productContainer.append(value)
        currentCart[item.id] = productContainer

    params['currentCart'] = currentCart

    return templater.render_to_response(request, 'shopping_cart.html', params)

# @permission_required('mainpage.is_user', login_url='/mainpage/login/')
@view_function
def add(request):
    params = {}

    if 'shopping_cart' not in request.session:
        request.session['shopping_cart'] = {}

    pid = request.urlparams[0]
    qty = int(request.urlparams[1])

    if pid in request.session['shopping_cart']:
        request.session['shopping_cart'][pid] += qty
        request.session.modified = True
    else: 
        request.session['shopping_cart'][pid] = qty
        request.session.modified = True

    return HttpResponseRedirect('/mainpage/shopping_cart/')

# @permission_required('mainpage.is_user', login_url='/mainpage/login/')
@view_function
def remove(request):
    pid = request.urlparams[0]

    del request.session['shopping_cart'][pid]
    request.session.modified = True

    return HttpResponseRedirect('/mainpage/shopping_cart/')


##Login
# @view_function
# def process_request(request):
#     params = {}

 
#     return templater.render_to_response(request,'shopping_cart.html',params)   

# @view_function
# def add(request):
#     params = {}
#     if 'shopping_cart' not in request.session:
#         request.session['shopping_cart'] = {}
#     pid = request.urlparams[0]
#     qty = request.urlparams[1]

#     if pid in request.session['shopping_cart']:
#         request.session['shopping_cart'][pid] += 1
#     else:
#         request.session['shopping_cart'][pid] = 1

#         request.session.modified = True

#         products = hmod.Product.objects.filter(id__in=request.session['shopping_cart'].keys)

#         params['products'] = products
#         params['shopping_cart'] = request.session['shopping_cart']
#         return templater.render_to_response(request, 'shopping_cart.html', params)

# #  @view_function
# # def show_shopping_cart(request):
# #     template_vars = {}
# #     itemids = request.session['shopping_cart'].keys()
# #     items = chfmod.ProductSpecification.objects.filter(id__in = itemids)
# #     template_vars['items'] = items
# #     return dmp_render_to_response(request, 'cart.html', template_vars)

# @view_function
# def delete(request):
#     params = {}
#     item = request.urlparams[0]
#     if request.method == 'DELETE':
#         del request.session['shopping_cart'][str(item)]
#         request.session.modified = True
#         return templater.render_to_response(request, 'shopping_cart.html', params)
#     HttpResponse(0)


