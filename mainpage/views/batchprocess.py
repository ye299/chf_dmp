from django.conf import settings
from django import forms
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as hmod
from django.shortcuts import render
from django_mako_plus.controller.router import get_renderer
import datetime
from django.contrib.auth.decorators import permission_required
from django.core.mail import send_mail, BadHeaderError

templater = get_renderer('mainpage')

@view_function
# @permission_required('is_active', login_url='/mainpage', raise_exception=False)
def process_request(request):
    params = {}
    now = datetime.datetime.now()
    today = datetime.date.today()

    #Create variables for the days overdue
    thirty = now - datetime.timedelta(days=30)
    sixty = now - datetime.timedelta(days=60)
    ninety = now - datetime.timedelta(days=90)

    #Grab the items that are thirty to sixty days overdue(30-59)
    sixty_days = hmod.Rental.objects.filter(dueDate__range=[sixty, thirty])
    params['sixty'] = sixty_days

    #Grab the items that are sixty to ninety days overdue(60-89)
    ninety_days = hmod.Rental.objects.filter(dueDate__range=[ninety, sixty])
    params['ninety'] = ninety_days

    # Grab the items that are ninety or more days overdue (90+)
    ninety_plus = hmod.Rental.objects.filter(dueDate__range=['1900-01-01', ninety])
    params['ninety_plus'] = ninety_plus
    params['rentals'] = hmod.Rental.objects.filter(returnTime=None)
    #Grab the items that are thirty days overdue
    params['today'] = today
    return templater.render_to_response(request, 'batchprocess.html', params)


@view_function
def send_email(request):
    try:
        user = hmod.User.objects.get(id=request.urlparams[0])
    except hmod.User.DoesNotExist:
        return HttpResponseRedirect('/mainpage')

    # rental = hmod.Rental.objects.filter(customer_id = request.urlparams[0], rentableItem = rental.id)

    print('Email sent')
    # print(rental)
    subject = 'Overdue Rental for' # + rental.id
    message = 'Dear Customer, your rental is overdue'
    from_email = 'yesukhei0822@gmail.com'
    to_email = [user.email]
    if subject and message and from_email:
        try:
            send_mail(subject, message, from_email, to_email)
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponseRedirect('/mainpage/batchprocess')
    else:
        return HttpResponse('/mainpage/batch')

