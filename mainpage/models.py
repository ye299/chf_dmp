from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User


class User(AbstractUser):

    security_question = models.TextField(null=True, blank=True)

    security_answer = models.TextField(null=True, blank=True)

    phone = models.TextField(max_length=20)

    organizationType = models.TextField(max_length=50)

    is_agent = models.NullBooleanField(null=True, blank=True)

    appointmentDate = models.DateField(null=True)



class Address(models.Model):

    address1 = models.TextField(max_length=100, null=True, blank=True)

    address2 = models.TextField(max_length=100, null=True, blank=True)

    city = models.TextField(max_length=50, null=True, blank=True)

    state = models.TextField(max_length=20, null=True, blank=True)

    ZIP = models.TextField(max_length=10, null=True, blank=True)

    # user = models.OneToOneField(User, null=True)
    #
    # event = models.ForeignKey(Event, related_name='+', null=True)
    def __str__(self):
        fulladdress = self.address1 + " " + self.address2 + " " + self.city + " " + self.state + " " + self.ZIP
        return fulladdress

    class Meta:
        abstract = True



class Photograph(models.Model):

    dateTaken = models.DateField(null=True, blank=True)

    placeTaken = models.TextField(null=True, blank=True)

    pictureFileName = models.TextField(null=True, blank=True)

    photographer = models.ForeignKey(User, null=True, blank=True)

    class Meta:
        abstract = True



class PhotographableThing(models.Model):

    Photo = models.ManyToManyField(Photograph)

    class Meta:
        abstract = True



class Item(Photograph):

    name = models.TextField(null=True, blank=True)

    description = models.TextField(null=True, blank=True)

    value = models.TextField(null=True, blank=True)

    standardPrice = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    person = models.ForeignKey(User, related_name='+', null=True, blank=True)



class RentableItem(Item):

    condition = models.TextField()

    # newDamage = models.TextField(null=True, blank=True)
    #
    # damageFee = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    #
    # lateFee = models.DecimalField(max_digits=10, decimal_places=2, null=True)

    available = models.NullBooleanField(null=True, blank=True)



class WardrobeItem(Item):

    size = models.TextField(null=True, blank=True)

    sizeMod = models.TextField(max_length=50, null=True, blank=True)

    gender = models.TextField(max_length=10, null=True, blank=True)

    color = models.TextField(max_length=50, null=True, blank=True)

    pattern = models.TextField(max_length=50, null=True, blank=True)

    startYear = models.TextField(max_length=50, null=True, blank=True)

    endYear = models.TextField(max_length=50, null=True, blank=True)

    note = models.TextField(null=True, blank=True)

    rentableWardrobeItem = models.OneToOneField(RentableItem, null=True, blank=True)



class Rental(models.Model):

    rentalTime = models.DateField(null=True, blank=True)

    dueDate = models.DateField(null=True, blank=True)

    discountPercent = models.IntegerField(null=True, blank=True)

    returnTime = models.DateField(null=True)

    # returned = models.NullBooleanField(null=True, blank=True)

    feesPaid = models.DecimalField(max_digits=10, decimal_places=2, null=True)

    customer = models.ForeignKey(User, related_name='+', null=True)

    agent = models.ForeignKey(User, related_name='+', null=True)

    rentalInstance = models.ManyToManyField(RentableItem, related_name='+', through='RentalInstance', null=True)

    def __str__(self):
        return (self.rentableItem, self.returnTime)



class RentalInstance(models.Model):

    Rental = models.ForeignKey(Rental)

    RentableItem = models.ForeignKey(RentableItem)

    # from RentableItem
    newDamage = models.TextField(null=True, blank=True)

    damageFee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    lateFee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    


class Event(Photograph, Address):

    name = models.TextField(max_length=50)

    description = models.TextField(null=True, blank=True)

    startDate = models.DateField(null=True, blank=True)

    endDate = models.DateField(null=True, blank=True)

    mapName = models.TextField(null=True, blank=True)

    venueName = models.TextField(null=True, blank=True)



class Area(Photograph):

    name = models.TextField(max_length=50)

    description = models.TextField(null=True, blank=True)

    placeNum = models.TextField(max_length=20)

    supervisor = models.ForeignKey(User, related_name='+', null=True)

    coordinator = models.ForeignKey(User, related_name='+', null=True)

    event = models.ForeignKey(Event, related_name='+', null=True, blank=True)



class Participant(User):

    sketch = models.TextField(null=True, blank=True)

    contactRel = models.TextField(null=True, blank=True)

    roleName = models.TextField(null=True, blank=True)

    roleType = models.TextField(null=True, blank=True)

    area = models.ManyToManyField(Area, related_name='+', null=True, blank=True)



class SaleItem(Item):

    lowPrice = models.DecimalField(max_digits=10, decimal_places=2)

    highPrice = models.DecimalField(max_digits=10, decimal_places=2)

    saleitem_areas = models.ForeignKey(Area, related_name='+', null=True)



class Product(Photograph):

    name = models.TextField(max_length=50)

    description = models.TextField(null=True, blank=True)

    category = models.TextField(max_length=50)

    unitPrice = models.DecimalField(max_digits=10, decimal_places=2)

    qtyOnHand = models.IntegerField()

    company = models.ForeignKey(User, related_name='+', null=True)



class Order(models.Model):

    orderDate = models.DateField(null=True, blank=True)

    phone = models.TextField(max_length=15)

    datePacked = models.DateField(null=True, blank=True)

    datePaid = models.DateField(null=True, blank=True)

    dateShipped = models.DateField(null=True, blank=True)

    trackingNum = models.TextField(null=True, blank=True)

    qty = models.IntegerField(null=True, blank=True)

    apiValue = models.TextField(null=True, blank=True)

    total = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    product = models.ManyToManyField(Product, null=True, blank=True)

    shippedBy = models.ForeignKey(User, related_name='+', null=True)

    customer = models.ForeignKey(User, related_name='+', null=True)