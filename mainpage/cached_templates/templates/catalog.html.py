# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428641674.122438
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/catalog.html'
_template_uri = 'catalog.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\t<h1 align="center">Online Catalog</h1>\r\n\r\n\t<div class="text-right">\r\n\t\tSearch Products: <input type="search" id="searchProducts"/>\r\n\t</div>\r\n\t</br>\r\n\t<h3>Products</h3>\r\n\r\n')
        for product in products:
            __M_writer('\t\t<div class="col-xs-18 col-sm-4 col-md-3">\r\n\t\t\t<main role="main">\r\n\t\t\t\t<div class="product">\r\n\t\t\t\t\t<figure>\r\n\t\t\t\t\t\t<img src="')
            __M_writer(str( STATIC_URL ))
            __M_writer('mainpage/media/')
            __M_writer(str(product.pictureFileName))
            __M_writer('" alt="Product Image" class="product-image">\r\n\t\t\t\t\t</figure>\r\n\r\n\t\t\t\t\t<div class="product-description">\r\n\r\n\t\t\t\t\t\t<div class="info">\r\n\t\t\t\t\t\t\t<h2>')
            __M_writer(str(product.name))
            __M_writer('</h2>\r\n\t\t\t\t\t\t\t<p class="pro_description">\r\n\t\t\t\t\t\t\t\t')
            __M_writer(str(product.description))
            __M_writer('\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t<div class="text-right">\r\n\t\t\t\t\t\t\t\tQuantity: <input id=\'#input1\' type="number" class="QTY" min="1" max="')
            __M_writer(str( product.qtyOnHand ))
            __M_writer('" value="1">\r\n\t\t\t\t\t\t\t\t<button data-pid= ')
            __M_writer(str( product.id ))
            __M_writer(' class="btn_addtocart btn btn-xs btn-warning"><i class="fa fa-cart-plus fontsize16"></i> ADD</button> \r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div class="price">\r\n\t\t\t\t\t\t\t')
            __M_writer(str(product.unitPrice))
            __M_writer('\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</main>\r\n\t\t</div>\r\n')
        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "catalog.html", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/catalog.html", "source_encoding": "ascii", "line_map": {"64": 25, "65": 25, "66": 28, "67": 28, "68": 29, "69": 29, "70": 34, "71": 34, "72": 41, "78": 72, "27": 0, "36": 1, "41": 42, "47": 3, "55": 3, "56": 12, "57": 13, "58": 17, "59": 17, "60": 17, "61": 17, "62": 23, "63": 23}}
__M_END_METADATA
"""
