# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428116226.929153
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/rental.overdue.html'
_template_uri = 'rental.overdue.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        rentals = context.get('rentals', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        rentals = context.get('rentals', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<h2 align="center">Overdue Rentals</h2>&nbsp;\r\n\r\n\t<div id=div1>\r\n\t<table class="table table-striped table-bordered">\r\n\t\t<tr>\r\n\t\t  <th>ID</th>\r\n\t\t  <th>Renter</th>\r\n\t\t  <th>Rental Date</th>\r\n\t\t  <th>Due Date</th>\r\n\t\t  <th>Discount Percent</th>\r\n\t\t  <th>Fee</th>\r\n\t\t  <th>Return</th>\r\n\t\t  <th>Agent</th>\r\n\t\t</tr>\r\n')
        for rental in rentals:	  
            __M_writer('\t\t<tr>\r\n\t\t  <td>')
            __M_writer(str( rental.id ))
            __M_writer('</td>\r\n\t\t  <td>')
            __M_writer(str( rental.customer))
            __M_writer('</td>\r\n\t\t  <td>')
            __M_writer(str( rental.rentalTime ))
            __M_writer('</td>\r\n\t\t  <th bgcolor = "#ff0000">')
            __M_writer(str( rental.dueDate ))
            __M_writer('</th>\r\n\t\t  <td>')
            __M_writer(str( rental.discountPercent ))
            __M_writer('%</td>\r\n\t\t  <td>')
            __M_writer(str( rental.feesPaid ))
            __M_writer('</td>\r\n\t\t  <td>')
            __M_writer(str( rental.returnTime ))
            __M_writer('</td>\r\n\t\t  <td>')
            __M_writer(str( rental.agent ))
            __M_writer('</td>\r\n\t\t</tr>\r\n')
        __M_writer('\t</table>\r\n\t</div>\r\n\r\n\t<div class="clearfix"></div>\r\n\t<div class="text-right">\r\n\t\t<a href="/mainpage/rental/" class="btn btn-primary">Back To Rentals</a>\r\n\t</div>&nbsp;\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "uri": "rental.overdue.html", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/rental.overdue.html", "line_map": {"64": 24, "65": 24, "66": 25, "67": 25, "68": 26, "69": 26, "70": 27, "71": 27, "72": 30, "78": 72, "27": 0, "35": 1, "40": 38, "46": 3, "53": 3, "54": 18, "55": 19, "56": 20, "57": 20, "58": 21, "59": 21, "60": 22, "61": 22, "62": 23, "63": 23}}
__M_END_METADATA
"""
