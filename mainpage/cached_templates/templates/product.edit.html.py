# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425230273.683511
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/product.edit.html'
_template_uri = 'product.edit.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        form = context.get('form', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        user = context.get('user', UNDEFINED)
        product = context.get('product', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n')
        if user and user.is_anonymous():
            __M_writer('\r\n')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
                context['self'].header(**pageargs)
            

            __M_writer('\r\n\r\n')
            if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
                context['self'].content(**pageargs)
            

            __M_writer('\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        form = context.get('form', UNDEFINED)
        product = context.get('product', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    \r\n    <form method="POST">\r\n        <table>\r\n            ')
        __M_writer(str(form))
        __M_writer('\r\n        </table>\r\n        <div style="margin-top:15px;">\r\n            <button class="btn btn-primary" type="submit">Submit</button>\r\n            <a href="/mainpage/product.delete/')
        __M_writer(str(product.id))
        __M_writer('/" class="btn btn-danger">Delete</a>\r\n            <button class="btn btn-default" type="cancel">Cancel</button>\r\n        </div>\r\n    </form>\r\n    \r\n   \r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Edit Product Information\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"65": 8, "66": 12, "67": 12, "68": 16, "69": 16, "39": 1, "40": 2, "41": 3, "75": 4, "46": 6, "81": 4, "51": 23, "87": 81, "57": 8, "27": 0}, "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/product.edit.html", "uri": "product.edit.html", "source_encoding": "ascii"}
__M_END_METADATA
"""
