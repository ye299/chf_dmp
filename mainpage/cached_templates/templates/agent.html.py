# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1423372532.013491
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/agent.html'
_template_uri = 'agent.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        agents = context.get('agents', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        agents = context.get('agents', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/agent.create/" class="btn btn-success btn-lg">Create a New agent</a>\r\n    \t\r\n    </div>\r\n\r\n    <table id=\'user_table\' class=\'table table-striped table-border table-condensed\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n    \t\t<th>First Name</th>\r\n    \t\t<th>Last Name</th>\r\n    \t\t<th>Email</th>\r\n            <th>Phone</th>\r\n    \t\t<th>Address</th>\r\n    \t\t<th>City</th>\r\n            <th>State</th>\r\n            <th>Zip</th>\r\n            <th>Organization</th>\r\n            <th>Creation Date</th>\r\n            <th>Appointment Date</th>\r\n            <th>Actions</th>\r\n    \t</tr>\r\n')
        for agent in agents:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(agent.id))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(agent.givenName))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(agent.familyName))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(agent.email))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.phone))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(agent.address))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.city))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.state))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.Zip))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.organizationType))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.creationDate))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(agent.appointmentDate))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/agent.edit/')
            __M_writer(str(agent.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Agents\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 34, "65": 34, "66": 35, "67": 35, "68": 36, "69": 36, "70": 37, "71": 37, "72": 38, "73": 38, "74": 39, "75": 39, "76": 40, "77": 40, "78": 41, "79": 41, "80": 42, "81": 42, "82": 43, "83": 43, "84": 44, "85": 44, "86": 45, "87": 45, "88": 48, "27": 0, "94": 3, "100": 3, "37": 1, "42": 5, "52": 8, "106": 100, "59": 8, "60": 31, "61": 32, "62": 33, "63": 33}, "source_encoding": "ascii", "uri": "agent.html", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/agent.html"}
__M_END_METADATA
"""
