# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425786303.465877
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/checkout.html'
_template_uri = 'checkout.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        product = context.get('product', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context)
        product = context.get('product', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<h2>Item Info</h2>\r\n\t<div class="wrapper">\r\n\t        <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/')
        __M_writer(str(product.picture))
        __M_writer('"/> \r\n\t        <div class="detail_container">\r\n\t            <h3>')
        __M_writer(str(product.name))
        __M_writer('</h3>\r\n\t            <div class="text-muted text-left">\r\n\t            Description: ')
        __M_writer(str(product.description))
        __M_writer('\r\n\t            </div>\r\n\t            <div class="text-muted text-left">\r\n\t            Category: ')
        __M_writer(str(product.category))
        __M_writer('\r\n\t            </div>\r\n\t            <div class="text-left">\r\n\t            Price:      $')
        __M_writer(str(product.unitPrice))
        __M_writer('\r\n\t            </div>\r\n\t\t\t</div>                \r\n\t    </div>\r\n\t\t</div>\r\n\t\t<h2>Customer Info</h2>\r\n\t\t<div class="cust_info">\r\n\t\t\t<h2>Payment Info</h2>\r\n\t\t\t<div id="pmt_info" class="text-left">\r\n\t\t\t\t<div>\r\n\t\t\t\t<label>First Name</label>\r\n\t\t\t\t<input type="text" size="20" autocomplete="off">\r\n\t\t\t\t<div>\r\n\t\t\t\t<label>Last Name</label>\r\n\t\t\t\t<input type="text" size="20" autocomplete="off">\r\n\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div>\r\n\t\t\t\t<label>Card Number</label>\r\n\t\t\t\t<input type="text" size="20" autocomplete="off">\r\n\t\t\t\t</div>\r\n\t\t\t\t<div>\r\n\t\t\t\t<span>Enter the number without spaces or hyphens.</span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div>\r\n\t\t\t\t<label>CVC</label>\r\n\t\t\t\t<input type="text" size="4" autocomplete="off">\r\n\t\t\t\t</div>\r\n\t\t\t\t<div>\r\n\t\t\t\t<label>Expiration (MM/YYYY)</label>\r\n\t\t\t\t<input type="text" size="2">\r\n\t\t\t\t<span> / </span>\r\n\t\t\t\t<input type="text" size="4">\r\n\t\t\t\t</div>\r\n\t\t\t\t<a href=""><img alt="" title="" src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/card.png/" width="500" height="100" border="0" /></a>\r\n\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<a type="button" href="/mainpage/product/" class="btn btn-default pull-right">Cancel</a>\r\n\t\t\t\t\t<a type="button" href="/mainpage/thankyou/" class="btn btn-success pull-right margin-right">Submit</a>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\r\n\t\t</div>\r\n\t</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n\tCheck Out\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "line_map": {"64": 10, "65": 10, "66": 12, "67": 12, "68": 14, "69": 14, "70": 17, "71": 17, "72": 20, "73": 20, "74": 54, "75": 54, "81": 2, "87": 2, "27": 0, "93": 87, "38": 1, "43": 4, "53": 7, "61": 7, "62": 10, "63": 10}, "uri": "checkout.html", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/checkout.html"}
__M_END_METADATA
"""
