# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1422517057.35859
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/user_edit.html'
_template_uri = 'user_edit.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        users = context.get('users', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        users = context.get('users', UNDEFINED)
        __M_writer = context.writer()
        __M_writer("\r\n    Edit\r\n    <table id='user_table' class='table table-striped table-border'>\r\n    \t<tr>\r\n    \t\t<th>First Name</th>\r\n    \t\t<th>Last Name</th>\r\n    \t\t<th>Email</th>\r\n    \t\t<th>Phone</th>\r\n    \t\t<th>City</th>\r\n    \t\t<th>State</th>\r\n    \t\t<th>Actions</th>\r\n    \t</tr>\r\n")
        for user in users:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(user.givenName))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.familyName))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.email))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.phone))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.city))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.state))
            __M_writer('</td>\r\n    \t\t\t<td>Edit | Delete</td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/user_edit.html", "line_map": {"64": 21, "65": 22, "66": 22, "35": 1, "73": 67, "45": 3, "27": 0, "67": 26, "52": 3, "53": 15, "54": 16, "55": 17, "56": 17, "57": 18, "58": 18, "59": 19, "60": 19, "61": 20, "62": 20, "63": 21}, "uri": "user_edit.html", "source_encoding": "ascii"}
__M_END_METADATA
"""
