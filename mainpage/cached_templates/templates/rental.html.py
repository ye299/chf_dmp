# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428643961.48173
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/rental.html'
_template_uri = 'rental.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        rentables = context.get('rentables', UNDEFINED)
        request = context.get('request', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        rentals = context.get('rentals', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        rentables = context.get('rentables', UNDEFINED)
        request = context.get('request', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        rentals = context.get('rentals', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if request.user.is_superuser:
            __M_writer('\t<div class="clearfix">\r\n\t\t<div class="text-right">\r\n\t\t\t<a href="/mainpage/rental.create/" class="btn btn-success">Create New Rental</a>\r\n\t\t\t<a href="/mainpage/batchprocess/" class="btn btn-default">Rental Report</a>\t\r\n\t\t</div>&nbsp;\r\n\r\n\t\t<table class="table table-bordered admin_table">\r\n\t\t\t<tr>\r\n\t\t\t  <th>ID</th>\r\n\t\t\t  <th>Renter</th>\t\t \r\n\t\t\t  <th>Rental Time</th>\r\n\t\t\t  <th>Return Time</th>\r\n\t\t\t  <th>Paid Fee</th>\r\n\t\t\t  <th>Due Date</th>\r\n\t\t\t  <th>Discount Percent</th>\r\n\t\t\t  <th>Agent</th>\r\n\t\t\t  <th>Action</th>\r\n\t\t\t</tr>\r\n')
            for rental in rentals:
                __M_writer('\t\t\t<tr>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.id ))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.customer))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.rentalTime ))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.returnTime ))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.feesPaid ))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.dueDate ))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.discountPercent ))
                __M_writer('</td>\r\n\t\t\t\t<td>')
                __M_writer(str( rental.agent ))
                __M_writer('</td>\r\n\t\t\t\t<td>\r\n\t\t\t\t\t<a href="/mainpage/rental.edit/')
                __M_writer(str( rental.id ))
                __M_writer('/" class="btn btn-xs btn-warning">Edit</a>\r\n')
                if rental.returnTime  == None:
                    __M_writer('\t\t\t\t\t<a href="/mainpage/rental.process_return/')
                    __M_writer(str( rental.id ))
                    __M_writer('/" class="btn btn-xs btn-primary">Return</a>\r\n')
                __M_writer('\t\t\t\t</td>\r\n\t\t\t</tr>\r\n')
            __M_writer('\t\t</table>\r\n\t</div>\r\n')
        __M_writer('\r\n\r\n')
        for rentable in rentables:
            __M_writer('\t\t<div class="col-xs-18 col-sm-4 col-md-3">\r\n\t\t\t<main role="main">\r\n\t\t\t\t<div class="product">\r\n\t\t\t\t\t<figure>\r\n\t\t\t\t\t\t<img src="')
            __M_writer(str( STATIC_URL ))
            __M_writer('mainpage/media/rental/')
            __M_writer(str(rentable.pictureFileName))
            __M_writer('" alt="Product Image" class="product-image">\r\n\t\t\t\t\t</figure>\r\n\r\n\t\t\t\t\t<div class="product-description">\r\n\r\n\t\t\t\t\t\t<div class="info">\r\n\t\t\t\t\t\t\t<h2>')
            __M_writer(str(rentable.name))
            __M_writer('</h2>\r\n\t\t\t\t\t\t\t<p class="pro_description">\r\n\t\t\t\t\t\t\t\t')
            __M_writer(str(rentable.description))
            __M_writer('\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t<div> Condition: ')
            __M_writer(str(rentable.condition))
            __M_writer('</div>\r\n\t\t\t\t\t\t\t<div class="text-right">\r\n\t\t\t\t\t\t\t\tQuantity: <input id=\'#input1\' type="number" class="QTY" min="1" value="1">\r\n\t\t\t\t\t\t\t\t<button data-pid = ')
            __M_writer(str( rentable.id ))
            __M_writer('\' class="btn_add_rental_tocart btn btn-xs btn-warning"><i class="fa fa-cart-plus fontsize16"></i> ADD</button> \r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div class="price">\r\n\t\t\t\t\t\t\t')
            __M_writer(str(rentable.standardPrice))
            __M_writer('\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</main>\r\n\t\t</div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"27": 0, "38": 1, "43": 77, "49": 3, "59": 3, "60": 5, "61": 6, "62": 24, "63": 25, "64": 26, "65": 26, "66": 27, "67": 27, "68": 28, "69": 28, "70": 29, "71": 29, "72": 30, "73": 30, "74": 31, "75": 31, "76": 32, "77": 32, "78": 33, "79": 33, "80": 35, "81": 35, "82": 36, "83": 37, "84": 37, "85": 37, "86": 39, "87": 42, "88": 45, "89": 47, "90": 48, "91": 52, "92": 52, "93": 52, "94": 52, "95": 58, "96": 58, "97": 60, "98": 60, "99": 62, "100": 62, "101": 65, "102": 65, "103": 70, "104": 70, "110": 104}, "source_encoding": "ascii", "uri": "rental.html", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/rental.html"}
__M_END_METADATA
"""
