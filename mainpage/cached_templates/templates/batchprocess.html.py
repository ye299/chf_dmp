# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428526070.44036
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF\\mainpage\\templates/batchprocess.html'
_template_uri = 'batchprocess.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        today = context.get('today', UNDEFINED)
        ninety = context.get('ninety', UNDEFINED)
        ninety_plus = context.get('ninety_plus', UNDEFINED)
        sixty = context.get('sixty', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        sixty = context.get('sixty', UNDEFINED)
        def content():
            return render_content(context)
        today = context.get('today', UNDEFINED)
        ninety = context.get('ninety', UNDEFINED)
        ninety_plus = context.get('ninety_plus', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <h3>Over due by 30-60 days</h3>\r\n   <table id="user_table" class="table admin_table table-bordered">\r\n        <tr>\r\n            <th>ID</td>\r\n            <th>Rental Time</td>\r\n            <th>Due Date</td>\r\n            <th>Overdue Days</td>\r\n            <th>Agent</td>\r\n            <th>Customer</td>\r\n            <th>Send Overdue Email</td>\r\n        </tr>\r\n')
        for six in sixty:
            __M_writer('        <tr>\r\n            <td>')
            __M_writer(str( six.id ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( six.rentalTime ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( six.dueDate ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str(today - six.dueDate ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( six.agent.first_name + " " + six.agent.last_name))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( six.customer.first_name + " " + six.customer.last_name))
            __M_writer('</td>\r\n            <td><a href="/mainpage/batchprocess.send_email/')
            __M_writer(str( six.customer_id ))
            __M_writer('" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-envelope"></span> Notify Customer</a>\r\n        </tr>\r\n')
        __M_writer('   </table>\r\n    <h3>Over due by 60-90 days</h3>\r\n   <table id="user_table" class="table admin_table table-bordered">\r\n        <tr>\r\n            <th>ID</td>\r\n            <th>Rental Time</td>\r\n            <th>Due Date</td>\r\n            <th>Overdue Days</td>\r\n            <th>Agent</td>\r\n            <th>Customer</td>\r\n            <th>Send Overdue Email</td>\r\n        </tr>\r\n')
        for nine in ninety:
            __M_writer('        <tr>\r\n            <td>')
            __M_writer(str( nine.id ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( nine.rentalTime ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( nine.dueDate ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str(today - nine.dueDate ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( nine.agent.first_name + " " + nine.agent.last_name))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( nine.customer.first_name + " " + six.customer.last_name))
            __M_writer('</td>\r\n            <td><a href="/mainpage/batchprocess.send_email/')
            __M_writer(str( nine.customer_id ))
            __M_writer('" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-envelope"></span> Notify Customer</a>\r\n        </tr>\r\n')
        __M_writer('    </table>\r\n    <h3>Over due by 90+ days</h3>\r\n   <table id="user_table" class="table admin_table table-bordered">\r\n        <tr>\r\n            <th>ID</td>\r\n            <th>Rental Time</td>\r\n            <th>Due Date</td>\r\n            <th>Overdue Days</td>\r\n            <th>Agent</td>\r\n            <th>Customer</td>\r\n            <th>Send Overdue Email</td>\r\n        </tr>\r\n')
        for ninet in ninety_plus:
            __M_writer('        <tr>\r\n            <td>')
            __M_writer(str( ninet.id ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( ninet.rentalTime ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( ninet.dueDate ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str(today - ninet.dueDate ))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( ninet.agent.first_name + " " + ninet.agent.last_name))
            __M_writer('</td>\r\n            <td>')
            __M_writer(str( ninet.customer.first_name + " " + ninet.customer.last_name))
            __M_writer('</td>\r\n            <td><a href="/mainpage/batchprocess.send_email/')
            __M_writer(str( ninet.customer_id ))
            __M_writer('" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-envelope"></span> Notify Customer</a>\r\n        </tr>\r\n')
        __M_writer('    </table>\r\n\r\n    <div class="clearfix"></div>\r\n    <div class="text-right">\r\n        <a href="/mainpage/rental/" class="btn btn-primary">Back To Rentals</a>\r\n    </div>&nbsp;\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    <h2>Late Rentals</h2>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:\\Python34\\Scripts\\CHF\\mainpage\\templates/batchprocess.html", "uri": "batchprocess.html", "source_encoding": "ascii", "line_map": {"128": 3, "134": 128, "27": 0, "40": 1, "45": 5, "55": 7, "65": 7, "66": 19, "67": 20, "68": 21, "69": 21, "70": 22, "71": 22, "72": 23, "73": 23, "74": 24, "75": 24, "76": 25, "77": 25, "78": 26, "79": 26, "80": 27, "81": 27, "82": 30, "83": 42, "84": 43, "85": 44, "86": 44, "87": 45, "88": 45, "89": 46, "90": 46, "91": 47, "92": 47, "93": 48, "94": 48, "95": 49, "96": 49, "97": 50, "98": 50, "99": 53, "100": 65, "101": 66, "102": 67, "103": 67, "104": 68, "105": 68, "106": 69, "107": 69, "108": 70, "109": 70, "110": 71, "111": 71, "112": 72, "113": 72, "114": 73, "115": 73, "116": 76, "122": 3}}
__M_END_METADATA
"""
