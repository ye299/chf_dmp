# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1427575308.530719
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/shopping_cart.html'
_template_uri = 'shopping_cart.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base_ajax.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\t<div>\r\n')
        for product in products:
            __M_writer('            <h3>')
            __M_writer(str(product.name))
            __M_writer('</h3>\r\n\t            <div class="text-muted text-left">\r\n\t            Description: ')
            __M_writer(str(product.description))
            __M_writer('\r\n\t            </div>\r\n\t            <div class="text-muted text-left">\r\n\t            Category: ')
            __M_writer(str(product.category))
            __M_writer('\r\n\t            </div>\r\n\t            <div class="text-left">\r\n\t            Price:      $')
            __M_writer(str(product.unitPrice))
            __M_writer('\r\n\t            </div>\r\n\t\t    <a type="button" data-pid=')
            __M_writer(str( product.id ))
            __M_writer(' href="/mainpage/shopping_cart.delete/')
            __M_writer(str( product.id ))
            __M_writer('" class="delete_btn btn-warning btn-xs">Delete</a>\r\n')
        __M_writer('\t</div>\r\n\t<a type="button" href="/mainpage/checkout/" class="btn btn-primary">Check Out</a>\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 18, "65": 18, "66": 18, "35": 1, "73": 67, "45": 4, "27": 0, "67": 20, "52": 4, "53": 7, "54": 8, "55": 8, "56": 8, "57": 10, "58": 10, "59": 13, "60": 13, "61": 16, "62": 16, "63": 18}, "source_encoding": "ascii", "uri": "shopping_cart.html", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/shopping_cart.html"}
__M_END_METADATA
"""
