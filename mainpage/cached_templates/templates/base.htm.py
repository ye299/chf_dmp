# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428641883.556343
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/base.htm'
_template_uri = 'base.htm'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['header', 'content', 'footer']


from django_mako_plus.controller import static_files 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        request = context.get('request', UNDEFINED)
        user = context.get('user', UNDEFINED)
        self = context.get('self', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def footer():
            return render_footer(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n')
        static_renderer = static_files.StaticRenderer(self) 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['static_renderer'] if __M_key in __M_locals_builtin_stored]))
        __M_writer('\r\n\r\n<!DOCTYPE html>\r\n<html>\r\n  <meta charset="UTF-8">\r\n  <head>\r\n    \r\n    <meta name="description" content="The Colonial Heritage Foundation is a non-profit corporation dedicated to the preservation of the values, culture, skills and history of America." />\r\n    <meta name="keywords" content="colonial, heritage, foundation, history, nonprofit, rental, festival" />\r\n    <meta name="author" content="Leo Enkhbaatar">\r\n    <meta name="robots" content="index, follow">\r\n    <meta name="revisit-after" content="3 month">\r\n    <title>Colonial Heritage Foundation</title>\r\n\r\n')
        __M_writer('    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>\r\n    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui-min.js"></script>\r\n    <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/jquery.form.js"></script>\r\n    <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/jquery.loadmodal.js"></script>\r\n    <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/scripts/index.js"></script>\r\n  \r\n\r\n    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">\r\n    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">\r\n\r\n    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>\r\n\r\n    <link rel="icon" type="image/x-icon" href="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/favicon.jpg" />\r\n    \r\n')
        __M_writer('    ')
        __M_writer(str( static_renderer.get_template_css(request, context)  ))
        __M_writer('\r\n  \r\n  </head>\r\n  \r\n    <div class="navbar-wrapper">\r\n      <div class="container">\r\n        <div class="navbar navbar-inverse navbar-static-top">\r\n          \r\n            <div class="navbar-header">\r\n          <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">\r\n            <span class="icon-bar"></span>\r\n            <span class="icon-bar"></span>\r\n            <span class="icon-bar"></span>\r\n          </a>\r\n            <a class="navbar-brand" href="/mainpage/index/">Colonial Heritage Foundation</a>\r\n            </div>\r\n            <div class="navbar-collapse collapse">\r\n              <ul class="nav navbar-nav">\r\n                <li><a href="/mainpage/catalog/">Catalog</a></li>\r\n                <li><a href="/mainpage/rental/">Rental</a></li> \r\n')
        if request.user.is_superuser:               
            __M_writer('                <li><a href="/mainpage/user/">User</a></li>\r\n')
        __M_writer('                <li class="dropdown">\r\n                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Event<b class="caret"></b></a>\r\n                  <ul class="dropdown-menu">\r\n                    <li><a href="/mainpage/event/">Event</a></li>\r\n                    <li><a href="/mainpage/publicevent/">Public Event</a></li>\r\n                  </ul>\r\n                </li>\r\n              </ul>\r\n              <ul class="nav navbar-nav navbar-right" style="margin-right:10px;">\r\n')
        if request.user.is_authenticated():
            __M_writer('                  <li><p class="pull-right userFullName" > Welcome, ')
            __M_writer(str(request.user.get_full_name()))
            __M_writer(' </p></li>\r\n                  <li><a href="/account/myaccount/')
            __M_writer(str(user.id))
            __M_writer('"><i class="fa fa-user"></i>  My Account</a></li>\r\n                  <li><a  href="#" id="cart_dialog"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>           \r\n                  <li><a href="/mainpage/index.logout/"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>\r\n')
        else:
            __M_writer('                  <li><a href="/account/signup.create/"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>\r\n                  <li><a href="" id="signin_btn" data-toggle="modal" data-target="#login_modal"><span class="glyphicon glyphicon-log-in"></span>  Login</a></li>\r\n')
        __M_writer('              </ul>\r\n            </div>\r\n        </div>\r\n      </div><!-- /container -->\r\n    </div><!-- /navbar wrapper -->\r\n\r\n    <div id="myCarousel" class="carousel slide">\r\n      <ol class="carousel-indicators">\r\n        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>\r\n      </ol>\r\n      <div class="carousel-inner">\r\n        <div class="item active">\r\n          <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/flag3.jpg" style="width:100%" class="img-responsive">\r\n          <div class="container">\r\n            <div class="carousel-caption">\r\n              <h1>Events</h1>\r\n              <p></p>\r\n              <p><a class="btn btn-lg btn-primary" href="/mainpage/event/">Learn More</a>\r\n              </p>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n\r\n    <!-- /Login Modal -->\r\n    <div id="login_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">\r\n      <div class="modal-dialog">\r\n        <div class="modal-content">\r\n          <div class="modal-header">\r\n            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>\r\n            <h1 class="text-center">Login</h1>\r\n          </div>\r\n          <div class="modal-body">\r\n           <!-- Django Form Fills the Body Here -->\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n\r\n    <body id="body">\r\n    \r\n      <div class="wrapper">\r\n        <header id="header">\r\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n        </header>\r\n\r\n        <div id="content">\r\n          ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('  \r\n        </div>\r\n      </div>\r\n\r\n      <div id="footer">\r\n        ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'footer'):
            context['self'].footer(**pageargs)
        

        __M_writer('\r\n      </div>\r\n')
        __M_writer('      ')
        __M_writer(str( static_renderer.get_template_js(request, context)  ))
        __M_writer('\r\n\r\n    </body>\r\n  \r\n</html>')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n            Site content goes here in sub-templates.\r\n          ')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_footer(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def footer():
            return render_footer(context)
        __M_writer = context.writer()
        __M_writer('\r\n            <div class="col-md-offset-5">\r\n              <ul class="nav navbar-nav">\r\n                <li><a href="">Contact</a></li>\r\n                <li><a href="">Terms</a></li>\r\n              </ul>\r\n            </div>\r\n        ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "base.htm", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/base.htm", "source_encoding": "ascii", "line_map": {"16": 4, "18": 0, "33": 2, "34": 4, "35": 5, "39": 5, "40": 20, "41": 22, "42": 22, "43": 23, "44": 23, "45": 24, "46": 24, "47": 32, "48": 32, "49": 35, "50": 35, "51": 35, "52": 55, "53": 56, "54": 58, "55": 67, "56": 68, "57": 68, "58": 68, "59": 69, "60": 69, "61": 72, "62": 73, "63": 76, "64": 88, "65": 88, "70": 124, "75": 130, "80": 142, "81": 145, "82": 145, "83": 145, "89": 122, "95": 122, "101": 128, "107": 128, "113": 135, "119": 135, "125": 119}}
__M_END_METADATA
"""
