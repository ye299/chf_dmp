# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1427955672.603998
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/catalog.detail_product.html'
_template_uri = 'catalog.detail_product.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        product = context.get('product', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        product = context.get('product', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\t<h1 align="center">')
        __M_writer(str( product.name ))
        __M_writer('</h1>\r\n\t</br>\r\n\t<div class="item_container text-center">\r\n\t<a href="/mainpage/catalog.detail_product/')
        __M_writer(str( product.id ))
        __M_writer('/"><img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/')
        __M_writer(str(product.pictureFileName))
        __M_writer('"/></a>&nbsp;\r\n\r\n\t \t<div class="Price_container">\r\n\t \t\t$ ')
        __M_writer(str( product.unitPrice ))
        __M_writer('\r\n\t \t</div>\r\n\t \t<div class="Description_container">\r\n\t \t\t ')
        __M_writer(str( product.description ))
        __M_writer('\r\n\t \t</div>&nbsp;\r\n\t \t<div class="clearfix"></div>\r\n\t \tQty: <input type="number" class="QTY">\r\n\t  <button data-pid=')
        __M_writer(str( product.id ))
        __M_writer(' class="btn_addtocart btn btn-warning">Add To Cart</button>\r\n\t</div>&nbsp;\r\n\r\n    \r\n\t<div id=\'div1\' class="text-right">\r\n\t\t<a href="/mainpage/catalog/" class="btn btn-primary">Back To Catalog</a>\r\n\t</div>&nbsp;\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/catalog.detail_product.html", "source_encoding": "ascii", "line_map": {"64": 11, "65": 11, "66": 14, "27": 0, "36": 1, "69": 18, "68": 18, "41": 25, "75": 69, "47": 3, "67": 14, "55": 3, "56": 5, "57": 5, "58": 8, "59": 8, "60": 8, "61": 8, "62": 8, "63": 8}, "uri": "catalog.detail_product.html"}
__M_END_METADATA
"""
