# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428525849.717255
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF\\mainpage\\templates/event.html'
_template_uri = 'event.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        events = context.get('events', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        events = context.get('events', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/event.create/" class="btn btn-success btn-lg">Create a New Event</a>\r\n    \r\n    </div>\r\n\r\n    <table class=\'table table-bordered admin_table\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n            <th>Name</th>\r\n            <th>Description</th>\r\n            <th>Map</th>\r\n            <th>Start Date</th>\r\n            <th>End Date</th>\r\n            <th>Actions</th>\r\n    \t</tr>\r\n')
        for event in events:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(event.id))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(event.name))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(event.description))
            __M_writer('</td>\r\n                <td><a href="/mainpage/event.info/')
            __M_writer(str( event.id ))
            __M_writer('/">')
            __M_writer(str( event.mapName ))
            __M_writer('</a></td>\r\n                <td>')
            __M_writer(str(event.startDate))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(event.endDate))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/event.edit/')
            __M_writer(str(event.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Events\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:\\Python34\\Scripts\\CHF\\mainpage\\templates/event.html", "uri": "event.html", "source_encoding": "ascii", "line_map": {"64": 28, "65": 28, "66": 29, "67": 29, "68": 30, "69": 30, "70": 30, "71": 30, "72": 31, "73": 31, "74": 32, "75": 32, "76": 33, "77": 33, "78": 36, "84": 3, "90": 3, "27": 0, "96": 90, "37": 1, "42": 5, "52": 8, "59": 8, "60": 25, "61": 26, "62": 27, "63": 27}}
__M_END_METADATA
"""
