# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428644235.828919
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/cart.html'
_template_uri = 'cart.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base_ajax.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        currentCart = context.get('currentCart', UNDEFINED)
        float = context.get('float', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        currentCart = context.get('currentCart', UNDEFINED)
        float = context.get('float', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\t\r\n\r\n<table class="table table-striped table-bordered">\r\n\t<tr>\r\n\t\t<th>Product</th>\r\n\t\t<th>QTY</th>\r\n\t\t<th>Price</th>\r\n\t\t<th>Subtotal</th>\r\n\t\t<th>Action</th>\r\n\t</tr>\r\n\r\n\t')
        grandTotal = 0
        
        __M_writer('\r\n\r\n')
        for key, value in currentCart.items():
            __M_writer('\t\t')

            price = float(value[0].unitPrice)
            qty = value[1]
            sub_total = (price * qty)
            grandTotal += sub_total
                            
            
            __M_writer('\r\n\t\t<tr>\r\n\t\t\t<td>')
            __M_writer(str( value[0].name ))
            __M_writer('</td>\r\n\t\t\t<td>')
            __M_writer(str( value[1] ))
            __M_writer('</td>\r\n\t\t\t<td>$ ')
            __M_writer(str( value[0].unitPrice ))
            __M_writer('</td>\r\n\t\t\t<td>$ ')
            __M_writer(str( sub_total ))
            __M_writer('</td>\r\n\t\t\t<td><button data-pid="')
            __M_writer(str( value[0].id ))
            __M_writer('" class="btn_removefromcart btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>\r\n\t\t</tr>\r\n')
        __M_writer('\t\t<tr>\r\n\t\t\t<td>Total:</td>\r\n\t\t\t<td></td>\r\n\t\t\t<td></td>\r\n\t\t\t<td></td>\r\n\t\t\t<td>$ ')
        __M_writer(str( grandTotal ))
        __M_writer('</td>\r\n\t\t</tr>\r\n\t</table>\r\n\t<div class="text-center">\r\n\t<a href="/mainpage/catalog.checkout/')
        __M_writer(str( grandTotal ))
        __M_writer('" class="btn btn-warning">Checkout</a>\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"68": 22, "69": 24, "70": 24, "71": 25, "72": 25, "73": 26, "74": 26, "75": 27, "76": 27, "77": 28, "78": 28, "79": 31, "80": 36, "81": 36, "82": 40, "83": 40, "89": 83, "27": 0, "36": 1, "41": 42, "47": 3, "55": 3, "56": 14, "58": 14, "59": 16, "60": 17, "61": 17}, "source_encoding": "ascii", "uri": "cart.html", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/cart.html"}
__M_END_METADATA
"""
