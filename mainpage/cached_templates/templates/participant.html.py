# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425189859.213151
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/participant.html'
_template_uri = 'participant.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        participants = context.get('participants', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        participants = context.get('participants', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/participant.create/" class="btn btn-success btn-lg">Create a New participant</a>\r\n    \t\r\n    </div>\r\n\r\n    <table id=\'user_table\' class=\'table table-striped table-border table-condensed\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n    \t\t<th>Sketch</th>\r\n    \t\t<th>Contact</th>\r\n    \t\t<th>Role Name</th>\r\n            <th>Role Type</th\r\n            <th>Actions</th>\r\n    \t</tr>\r\n')
        for participant in participants:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(participant.id))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(participant.sketch))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(participant.contactRel))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(participant.roleName))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(participant.roleType))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/participant.edit/')
            __M_writer(str(participant.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Partcipants\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "participant.html", "line_map": {"64": 27, "65": 27, "66": 28, "67": 28, "68": 29, "69": 29, "70": 30, "71": 30, "72": 31, "73": 31, "74": 34, "80": 3, "86": 3, "27": 0, "92": 86, "37": 1, "42": 5, "52": 8, "59": 8, "60": 24, "61": 25, "62": 26, "63": 26}, "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/participant.html", "source_encoding": "ascii"}
__M_END_METADATA
"""
