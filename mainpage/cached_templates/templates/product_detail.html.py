# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425773101.255589
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/product_detail.html'
_template_uri = 'product_detail.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        product = context.get('product', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        product = context.get('product', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    </div>\r\n    <div>\r\n        <table id=\'user_table\'>\r\n             <tr>\r\n                <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/')
        __M_writer(str(product.picture))
        __M_writer('"/>\r\n            </tr>\r\n            <tr>\r\n                <td>')
        __M_writer(str(product.name))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <td>')
        __M_writer(str(product.description))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <td>')
        __M_writer(str(product.unitPrice))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <a class="btn btn-warning" href="/account/myaccount.edit/')
        __M_writer(str(user.id))
        __M_writer('">Edit</a>\r\n                <a class="btn btn-primary" href="/account/myaccount.changepassword/')
        __M_writer(str(user.id))
        __M_writer('">Change Password</a>\r\n            </tr>\r\n\r\n        </table>\r\n    </div>\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Products\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 15, "65": 15, "66": 15, "67": 15, "68": 18, "69": 18, "70": 21, "71": 21, "72": 24, "73": 24, "74": 27, "75": 27, "76": 28, "77": 28, "83": 3, "89": 3, "27": 0, "95": 89, "39": 1, "44": 5, "54": 8, "63": 8}, "uri": "product_detail.html", "source_encoding": "ascii", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/product_detail.html"}
__M_END_METADATA
"""
