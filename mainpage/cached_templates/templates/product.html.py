# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1427575006.026295
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/product.html'
_template_uri = 'product.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['header', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        products = context.get('products', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n\r\n<form method="get" action="/mainpage/product.search/">\r\n            <label for="id_q">Search:</label>\r\n            <input type="text" name="q" id="id_q"/>\r\n            <input type="submit" value="Submit"/>\r\n            <li> {{ found_entries }}}</li>\r\n        </form>')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Products\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <div class="clearfix"></div>\r\n        \r\n        <div class="text-right">\r\n            Search Products: <input type="search" id="searchProducts"/>\r\n        </div>\r\n\r\n')
        for product in products:
            __M_writer('            <div class="item_container">\r\n                <div class="product_info">\r\n                    <a id="img" href="/mainpage/product.productdetail/')
            __M_writer(str(product.id))
            __M_writer('">\r\n                        <img src="')
            __M_writer(str( STATIC_URL ))
            __M_writer('mainpage/media/')
            __M_writer(str(product.picture))
            __M_writer('"/>\r\n                        <h3>')
            __M_writer(str(product.name))
            __M_writer('</h3>\r\n                        <div class="text-muted text-left">\r\n                        Description: ')
            __M_writer(str(product.description))
            __M_writer('\r\n                        </div>\r\n                        <div class="text-muted text-left">\r\n                        Category: ')
            __M_writer(str(product.category))
            __M_writer('\r\n                        </div>\r\n                        <div class="text-left">\r\n                        Price:      $')
            __M_writer(str(product.unitPrice))
            __M_writer('\r\n                        </div>\r\n                    </a>\r\n                </div>    \r\n\r\n                <div class="product_btns">\r\n                        Qty: <input id=\'#input1\' type="number" class="QTY">\r\n                        <button data-pid=')
            __M_writer(str( product.id ))
            __M_writer(' class="btn_addtocart btn btn-xs btn-warning">Add To Cart</button> \r\n\r\n                        <button data-pid="')
            __M_writer(str(product.id))
            __M_writer('" class="add_btn btn btn-md btn-success">Add To Cart</button>            \r\n                        <a href="/mainpage/product.productdetail/')
            __M_writer(str(product.id))
            __M_writer('" class=" btn btn-md btn-primary"> Info</a>            \r\n                        <a href="/mainpage/product.checkout/')
            __M_writer(str(product.id))
            __M_writer('" class=" btn btn-md btn-warning">Buy Now</a>            \r\n                </div>\r\n            </div>  \r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"66": 8, "74": 8, "75": 15, "76": 16, "77": 18, "78": 18, "79": 19, "80": 19, "81": 19, "82": 19, "83": 20, "84": 20, "85": 22, "86": 22, "87": 25, "88": 25, "89": 28, "90": 28, "27": 0, "92": 35, "93": 37, "94": 37, "95": 38, "96": 38, "97": 39, "98": 39, "91": 35, "38": 1, "104": 98, "43": 5, "48": 43, "54": 3, "60": 3}, "source_encoding": "ascii", "uri": "product.html", "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/product.html"}
__M_END_METADATA
"""
