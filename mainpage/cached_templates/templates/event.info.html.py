# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428008008.834776
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/event.info.html'
_template_uri = 'event.info.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        event = context.get('event', UNDEFINED)
        areas = context.get('areas', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        sales = context.get('sales', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        event = context.get('event', UNDEFINED)
        areas = context.get('areas', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        sales = context.get('sales', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div>\r\n\t\t<h2 align="center">')
        __M_writer(str( event.mapName ))
        __M_writer(': ')
        __M_writer(str(event.startDate))
        __M_writer(' ~ ')
        __M_writer(str(event.endDate))
        __M_writer('</h2>\r\n\t\t<h3>')
        __M_writer(str(event.description))
        __M_writer('</h3>\r\n\t</div>\r\n\t</br>\r\n')
        for area in areas:
            __M_writer('\r\n\t<div class="item_container text-center">\r\n\t<a href="/mainpage/event.info/')
            __M_writer(str( event.id ))
            __M_writer('/"><img src="')
            __M_writer(str( STATIC_URL ))
            __M_writer('mainpage/media/')
            __M_writer(str(area.pictureFileName))
            __M_writer('"/></a>&nbsp;\r\n\r\n\t \t<div class="Price_container">\r\n\t \t\t ')
            __M_writer(str( area.name ))
            __M_writer('\r\n\t \t</div>\r\n\t \t<div class="Description_container">\r\n\t \t\t ')
            __M_writer(str( area.description ))
            __M_writer('\r\n\t \t</div>&nbsp;\r\n\r\n')
            for sale in sales:
                __M_writer('\t \t<a href="#"><img src="')
                __M_writer(str( STATIC_URL ))
                __M_writer('mainpage/media/')
                __M_writer(str(sale.pictureFileName))
                __M_writer('"/></a>&nbsp;\r\n\r\n\t \t<div class="Price_container">\r\n\t \t\t ')
                __M_writer(str( sale.name ))
                __M_writer('\r\n\t \t</div>\r\n\t \t<div class="Description_container">\r\n\t \t\t ')
                __M_writer(str( sale.description ))
                __M_writer('\r\n\t \t</div>\r\n\t \t<div>\r\n\t \t\t ')
                __M_writer(str( sale.lowPrice ))
                __M_writer(' ~ ')
                __M_writer(str( sale.highPrice ))
                __M_writer('\r\n\t\t</div>\t \t\t\r\n\t \t&nbsp;\r\n\r\n\t \t<div class="clearfix"></div>\r\n\t \tQty: <input type="number" class="QTY">\r\n\t  <button data-pid=')
                __M_writer(str( event.id ))
                __M_writer(' class="btn_addtocart btn btn-warning">Add To Cart</button>\r\n\t</div>&nbsp;\r\n')
        __M_writer('\r\n    \r\n\t<div id=\'div1\' class="text-right">\r\n\t\t<a href="/mainpage/event/" class="btn btn-primary">Back To Catalog</a>\r\n\t</div>&nbsp;\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "line_map": {"64": 5, "65": 5, "66": 6, "67": 6, "68": 9, "69": 10, "70": 12, "71": 12, "72": 12, "73": 12, "74": 12, "75": 12, "76": 15, "77": 15, "78": 18, "79": 18, "80": 21, "81": 22, "82": 22, "83": 22, "84": 22, "85": 22, "86": 25, "87": 25, "88": 28, "89": 28, "90": 31, "27": 0, "92": 31, "93": 31, "94": 37, "95": 37, "96": 41, "91": 31, "102": 96, "38": 1, "43": 46, "49": 3, "59": 3, "60": 5, "61": 5, "62": 5, "63": 5}, "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/event.info.html", "uri": "event.info.html"}
__M_END_METADATA
"""
