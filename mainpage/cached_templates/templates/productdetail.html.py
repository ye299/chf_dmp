# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425794608.735051
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/productdetail.html'
_template_uri = 'productdetail.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        product = context.get('product', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        product = context.get('product', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n    <div class="wrapper">\r\n        <div class="pic_container">\r\n            <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('mainpage/media/')
        __M_writer(str(product.picture))
        __M_writer('"/> \r\n        </div>   \r\n        <div class="detail_container">\r\n            <h3>')
        __M_writer(str(product.name))
        __M_writer('</h3>\r\n            <div class="text-muted text-left">\r\n            Description: ')
        __M_writer(str(product.description))
        __M_writer('\r\n            </div>\r\n            <div class="text-muted text-left">\r\n            Category: ')
        __M_writer(str(product.category))
        __M_writer('\r\n            </div>\r\n            <div class="text-left">\r\n            Price:      $')
        __M_writer(str(product.unitPrice))
        __M_writer('\r\n            </div>\r\n            <div class="text-right">\r\n                <button data-pid="')
        __M_writer(str(product.id))
        __M_writer('" class="add_btn btn btn-md btn-primary">Add To Cart</button>\r\n                <a href="/mainpage/product.checkout/')
        __M_writer(str(product.id))
        __M_writer('" class=" btn btn-md btn-warning">Buy Now</a>            \r\n            </div>\r\n        </div>                \r\n    </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        product = context.get('product', UNDEFINED)
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    ')
        __M_writer(str(product.name))
        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "ascii", "uri": "productdetail.html", "line_map": {"64": 11, "65": 11, "66": 14, "67": 14, "68": 16, "69": 16, "70": 19, "71": 19, "72": 22, "73": 22, "74": 25, "75": 25, "76": 26, "77": 26, "83": 3, "90": 3, "27": 0, "92": 4, "98": 92, "91": 4, "38": 1, "43": 5, "53": 8, "61": 8, "62": 11, "63": 11}, "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/productdetail.html"}
__M_END_METADATA
"""
