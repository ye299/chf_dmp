# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428204696.882344
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF\\mainpage\\templates/rental.process_return.html'
_template_uri = 'rental.process_return.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        form = context.get('form', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\t<form method="POST" align="center">\r\n\t <table style="margin: 0px auto;">\r\n\t ')
        __M_writer(str( form ))
        __M_writer('\r\n \t </table>\r\n \t <br>\r\n \t <button type="submit" class="btn btn-primary">Submit</button>\r\n \t <a href="/mainpage/rental/" class="btn btn-danger">Cancel</a>\r\n \t</form>\t\r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    <h2>Rental Return</h2>\r\n    \r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "c:\\Python34\\Scripts\\CHF\\mainpage\\templates/rental.process_return.html", "uri": "rental.process_return.html", "source_encoding": "ascii", "line_map": {"67": 3, "27": 0, "52": 8, "37": 1, "73": 3, "42": 6, "59": 8, "60": 12, "61": 12, "79": 73}}
__M_END_METADATA
"""
