# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425710842.210288
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/productss.html'
_template_uri = 'productss.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['header', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Products\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        products = context.get('products', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/product.create/" class="btn btn-success btn-lg">Add a Product</a>\r\n    \r\n    </div>\r\n\r\n    <table id=\'user_table\' class=\'table table-striped table-border table-condensed\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n    \t\t<th>Name</th>\r\n    \t\t<th>Description</th>\r\n    \t\t<th>Category</th>\r\n    \t\t<th>Unit Price</th>\r\n            <th>Qty</th>\r\n    \t\t<th>Actions</th>\r\n    \t</tr>\r\n')
        for product in products:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(product.id))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(product.name))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(product.description))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(product.category))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(product.unitPrice))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(product.qtyOnHand))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/product.edit/')
            __M_writer(str(product.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 8, "71": 8, "72": 25, "73": 26, "74": 27, "75": 27, "76": 28, "77": 28, "78": 29, "79": 29, "80": 30, "81": 30, "82": 31, "83": 31, "84": 32, "85": 32, "86": 33, "87": 33, "88": 36, "27": 0, "94": 88, "37": 1, "42": 5, "52": 3, "58": 3}, "uri": "productss.html", "source_encoding": "ascii", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/productss.html"}
__M_END_METADATA
"""
