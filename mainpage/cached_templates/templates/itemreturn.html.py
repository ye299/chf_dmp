# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1423341760.096309
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/itemreturn.html'
_template_uri = 'itemreturn.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        itemreturns = context.get('itemreturns', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        itemreturns = context.get('itemreturns', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/itemreturn.create/" class="btn btn-success btn-lg">Return an Item</a>\r\n    \t<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#loginModal">\r\n\t      Login\r\n\t    </button>\r\n    </div>\r\n\r\n    <table id=\'user_table\' class=\'table table-striped table-border table-condensed\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n            <th>itemreturn Date</th>\r\n            <th>Fee</th>\r\n            <th>Actions</th>\r\n    \t</tr>\r\n')
        for itemreturn in itemreturns:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(itemreturn.id))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(itemreturn.returnTime))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(itemreturn.feesPaid))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/itemreturn.edit/')
            __M_writer(str(itemreturn.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n   Item Returns\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 27, "65": 27, "66": 28, "59": 8, "68": 29, "37": 1, "70": 32, "42": 5, "76": 3, "82": 3, "67": 28, "52": 8, "69": 29, "88": 82, "27": 0, "60": 24, "61": 25, "62": 26, "63": 26}, "uri": "itemreturn.html", "source_encoding": "ascii", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/itemreturn.html"}
__M_END_METADATA
"""
