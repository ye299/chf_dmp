# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1423372540.739688
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/photograph.html'
_template_uri = 'photograph.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        def header():
            return render_header(context._locals(__M_locals))
        photographs = context.get('photographs', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        photographs = context.get('photographs', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/photograph.create/" class="btn btn-success btn-lg">Create a New photograph</a>\r\n\r\n    </div>\r\n\r\n    <table id=\'user_table\' class=\'table table-striped table-border table-condensed\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n            <th>Date Taken</th>\r\n            <th>Place</th>\r\n            <th>Image</th>\r\n            <th>Actions</th>\r\n    \t</tr>\r\n')
        for photograph in photographs:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(photograph.id))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(photograph.dateTaken))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(photograph.placeTaken))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(photograph.image))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/photograph.edit/')
            __M_writer(str(photograph.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Photographs\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 26, "65": 26, "66": 27, "59": 8, "68": 28, "37": 1, "70": 29, "71": 29, "72": 32, "42": 5, "78": 3, "67": 27, "52": 8, "69": 28, "84": 3, "90": 84, "27": 0, "60": 23, "61": 24, "62": 25, "63": 25}, "source_encoding": "ascii", "uri": "photograph.html", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/photograph.html"}
__M_END_METADATA
"""
