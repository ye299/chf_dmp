# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1425189862.218138
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/participant.edit.html'
_template_uri = 'participant.edit.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        form = context.get('form', UNDEFINED)
        participant = context.get('participant', UNDEFINED)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n\r\n\r\n\r\n\r\n\r\n')
        for field in form:
            __M_writer('            <div class="form-group">\r\n                <label for=')
            __M_writer(str(field.name))
            __M_writer('>')
            __M_writer(str(field.name))
            __M_writer('</label>\r\n                ')
            __M_writer(str(field))
            __M_writer('\r\n            </div>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        form = context.get('form', UNDEFINED)
        participant = context.get('participant', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n    \r\n    <form method="POST">\r\n        <table>\r\n            ')
        __M_writer(str(form))
        __M_writer('\r\n        </table>\r\n        <div style="margin-top:15px;">\r\n            <button class="btn btn-primary" type="submit">Submit</button>\r\n            <a href="/mainpage/participant.delete/')
        __M_writer(str(participant.id))
        __M_writer('/" class="btn btn-danger">Delete participant</a>\r\n            <button class="btn btn-default" type="cancel">Cancel</button>\r\n        </div>\r\n    </form>\r\n    \r\n   \r\n\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Edit Participant Information\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "participant.edit.html", "line_map": {"80": 4, "70": 8, "38": 1, "71": 12, "72": 12, "73": 16, "74": 16, "43": 6, "92": 86, "48": 23, "49": 30, "50": 31, "51": 32, "52": 32, "53": 32, "54": 32, "55": 33, "56": 33, "27": 0, "86": 4, "62": 8}, "filename": "C:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/participant.edit.html", "source_encoding": "ascii"}
__M_END_METADATA
"""
