# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1423340345.318015
_enable_loop = True
_template_filename = 'c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/Return.html'
_template_uri = 'Return.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['header', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        returns = context.get('returns', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Returns\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        returns = context.get('returns', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/return.create/" class="btn btn-success btn-lg">Create a New Return</a>\r\n    \t<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#loginModal">\r\n\t      Login\r\n\t    </button>\r\n    </div>\r\n\r\n    <table id=\'user_table\' class=\'table table-striped table-border table-condensed\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n            <th>Return Date</th>\r\n            <th>Fee</th>\r\n            <th>Actions</th>\r\n    \t</tr>\r\n')
        for Return in returns:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(Return.id))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(Return.returnTime))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(Return.feesPaid))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/return.edit/')
            __M_writer(str(Return.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"line_map": {"64": 8, "37": 1, "71": 8, "72": 24, "73": 25, "74": 26, "75": 26, "76": 27, "77": 27, "78": 28, "79": 28, "80": 29, "81": 29, "82": 32, "52": 3, "88": 82, "58": 3, "27": 0, "42": 5}, "uri": "Return.html", "filename": "c:\\Python34\\Scripts\\CHF_DMP\\mainpage\\templates/Return.html", "source_encoding": "ascii"}
__M_END_METADATA
"""
