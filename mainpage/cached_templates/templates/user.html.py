# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1428525671.738732
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF\\mainpage\\templates/user.html'
_template_uri = 'user.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['content', 'header']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        users = context.get('users', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        users = context.get('users', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    \t<a href="/mainpage/user.create/" class="btn btn-success btn-lg">Create a New User</a>\r\n   \r\n    </div>\r\n\r\n    <table id=\'\' class=\'table table-bordered\'>\r\n    \t<tr>\r\n    \t\t<th>ID</th>\r\n    \t\t<th>User Name</th>\r\n    \t\t<th>First Name</th>\r\n    \t\t<th>Last Name</th>\r\n    \t\t<th>Email</th>\r\n    \t\t<th>Last Login</th>\r\n    \t\t<th>Actions</th>\r\n    \t</tr>\r\n')
        for user in users:
            __M_writer('    \t\t<tr>\r\n    \t\t\t<td>')
            __M_writer(str(user.id))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.username))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.first_name))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.last_name))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.email))
            __M_writer('</td>\r\n    \t\t\t<td>')
            __M_writer(str(user.last_login))
            __M_writer('</td>\r\n    \t\t\t<td><a href="/mainpage/user.edit/')
            __M_writer(str(user.id))
            __M_writer('">Edit</a></td>\r\n    \t\t</tr>\r\n')
        __M_writer('    </table>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    Users\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:\\Python34\\Scripts\\CHF\\mainpage\\templates/user.html", "uri": "user.html", "source_encoding": "ascii", "line_map": {"64": 28, "65": 28, "66": 29, "67": 29, "68": 30, "69": 30, "70": 31, "71": 31, "72": 32, "73": 32, "74": 33, "75": 33, "76": 36, "82": 3, "88": 3, "27": 0, "94": 88, "37": 1, "42": 5, "52": 8, "59": 8, "60": 25, "61": 26, "62": 27, "63": 27}}
__M_END_METADATA
"""
