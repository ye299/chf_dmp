from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User


class User(AbstractUser):

    # is_staff = models.BooleanField(default=False)

    # is_superuser = models.BooleanField(default=False)

    # is_active = models.BooleanField(default=True)

    securityQuestion = models.TextField(null=True, blank=True)

    securityAnswer = models.TextField(null=True, blank=True)

    phone = models.TextField(max_length=20)

    organizationType = models.TextField(max_length=50)

    is_agent = models.NullBooleanField(null=True, blank=True)

    appointmentDate = models.DateField(null = True, blank = True)



class Photograph(models.Model):

    dateTaken = models.DateField(null=True, blank=True)

    placeTaken = models.TextField(null=True, blank=True)

    image = models.TextField()

    photographer = models.ForeignKey(User)



class PhotographableThing(models.Model):

    Photo = models.ManyToManyField(Photograph)



class Rental(models.Model):

    rentalTime = models.DateField(null=True, blank=True)

    dueDate = models.DateField(null=True, blank=True)

    discountPercent = models.IntegerField(null=True, blank=True)
    rentalPrice = models.DecimalField(max_digits=5, decimal_places=2)
    agentName =  models.TextField(max_length=50)
    personName = models.TextField(max_length=50)
    address = models.TextField(max_length=50)
    city = models.TextField(max_length=50)
    state = models.TextField(max_length=50)
    zip = models.TextField(max_length=50)
    email = models.TextField(max_length=50)
    phone = models.TextField(max_length=50)
    rentedItem = models.TextField(max_length=50)

    returnTime = models.DateField(null=True, blank=True)

    returned = models.NullBooleanField(null=True, blank=True)

    feesPaid = models.DecimalField(max_digits=10, decimal_places=2)

    # customer = models.ForeignKey(User, related_name='+')

    # agent = models.ForeignKey(User, related_name='+')

    def __str__(self):
        return (self.rentableItem, self.returnTime)



class RentableItem(models.Model):

    condition = models.TextField()

    newDamage = models.TextField(null=True, blank=True)

    damageFee = models.DecimalField(max_digits=10, decimal_places=2)

    lateFee = models.DecimalField(max_digits=10, decimal_places=2)

    rental = models.ManyToManyField(Rental, related_name='+')



class Item(models.Model):

    name = models.TextField()

    description = models.TextField()

    value = models.TextField()

    standardPrice = models.DecimalField(max_digits=10, decimal_places=2)

    rentableItem = models.ForeignKey(RentableItem, related_name='+')

    person = models.ForeignKey(User, related_name='+')



class WardrobeItem(models.Model):

    size = models.IntegerField()

    sizeMod = models.TextField(max_length=50)

    gender = models.TextField(max_length=10)

    color = models.TextField(max_length=50)

    pattern = models.TextField(max_length=50)

    startYear = models.DateField(null = True, blank = True)

    endYear = models.DateField(null = True, blank = True)

    note = models.TextField(null=True, blank=True)

    rentableItem = models.ForeignKey(RentableItem, related_name='+')

    item = models.ForeignKey(Item, related_name='+')



class Event(models.Model):

    startDate = models.DateField(null = True, blank = True)

    endDate = models.DateField(null = True, blank = True)

    mapName = models.TextField()

    venueName = models.TextField(null=True, blank=True)

    # public_event = models.BooleanField(default=None)


class Address(models.Model):
    
    address1 = models.TextField(max_length=100)

    address2 = models.TextField(max_length=100, null=True, blank=True)

    city = models.TextField(max_length=50)

    state = models.TextField(max_length=20)

    ZIP  = models.TextField(max_length=10)

    user = models.OneToOneField(User)

    event = models.ForeignKey(Event, related_name='+') 

    def __str__(self):
        Address = self.address1 + self.address2 + self.city + self.state + self.ZIP
        return (Address)


class Area(models.Model):

    name = models.TextField(max_length=50)

    description = models.TextField(null=True, blank=True)

    placeNum = models.TextField(max_length=20)

    supervisor = models.ForeignKey(User, related_name='+')

    coordinator = models.ForeignKey(User, related_name='+')

    event = models.ForeignKey(Event, related_name='+')


class Participant(models.Model):

    sketch = models.TextField(null=True, blank=True)

    contactRel = models.TextField(null=True, blank=True)

    IDphoto = models.TextField(null=True, blank=True)

    roleName = models.TextField(null=True, blank=True)

    roleType = models.TextField(null=True, blank=True)

    person = models.ForeignKey(User, related_name='+')

    area = models.ManyToManyField(Area, related_name='+')



class SaleItem(models.Model):

    description = models.TextField(null=True, blank=True)

    lowPrice = models.DecimalField(max_digits=10, decimal_places=2)

    highPrice = models.DecimalField(max_digits=10, decimal_places=2)

    location = models.ForeignKey(Area, related_name='+')



# class PublicEvent(models.Model):

#     name = models.TextField(max_length=50)

#     description = models.TextField()

#     event = models.ForeignKey(Event, related_name='+')



class Order(models.Model):

    orderDate = models.DateField(null = True, blank = True)

    phone = models.TextField(max_length=15)

    datePacked = models.DateField(null = True, blank = True)

    datePaid = models.DateField(null = True, blank = True)

    dateShipped = models.DateField(null = True, blank = True)

    trackingNum = models.TextField()

    qty = models.IntegerField()

    price = models.DecimalField(max_digits=10, decimal_places=2)

    shippedBy = models.ForeignKey(User, related_name='+')

    customer = models.ForeignKey(User, related_name='+')



class Product(models.Model):

    name = models.TextField(max_length=50)

    description = models.TextField(null=True, blank=True)

    category = models.TextField(max_length=50)

    unitPrice = models.DecimalField(max_digits=10, decimal_places=2)

    qtyOnHand = models.IntegerField()

    picture = models.TextField()

    company = models.ForeignKey(User, related_name='+')

    order = models.ForeignKey(Order, related_name='+')



