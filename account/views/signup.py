from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as amod
from django_mako_plus.controller.router import get_renderer
from django import forms
from django.contrib.auth.decorators import permission_required
import random
import datetime

templater = get_renderer('account')

##Pull users from DB
@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def process_request(request):
    params = {}

    params['accounts'] = amod.User.objects.all().order_by('id')
    return templater.render_to_response(request,'myaccount.html',params)

@view_function
def check_username(request):
    username = request.REQUEST.get('u')
    try:
        user = amod.User.objects.get(username=username)
    except amod.User.DoesNotExist:
        return HttpResponse('0')  # username is available, not in DB
    return HttpResponse('1')  # username is not available, in DB


##Edit Users
@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def edit(request):
    params = {}

    try:
        user = amod.User.objects.get(id=request.urlparams[0])
    except amod.User.DoesNotExist:
        return HttpResponseRedirect('/account/signup/')

    form = SignUpForm(initial={
        # 'password': user.password ,
        'last_login': user.last_login ,
        'username': user.username ,
        'first_name': user.first_name ,
        'last_name': user.last_name , 
        'email': user.email ,
        'phone': user.phone ,
        'organizationType': user.organizationType ,
        'security_question': user.security_question,
        'security_answer': user.security_answer,

        })

    if request.method == 'POST':
     form = SignUpForm(request.POST)
     form.userid = user.id
     if form.is_valid():
        user.set_password(form.cleaned_data['password'])
        user.username = form.cleaned_data['username']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.email = form.cleaned_data['email']
        user.phone = form.cleaned_data['phone']
        user.security_question = form.cleaned_data['security_question']
        user.security_answer = form.cleaned_data['security_answer']
        user.organizationType = form.cleaned_data['organizationType']
        user.save()
        return HttpResponseRedirect('/account/signup/')

    params['form'] = form
    params['user'] = user

    return templater.render_to_response(request,'signup.edit.html',params)    

class SignUpForm(forms.Form):

    username = forms.CharField(required=True, max_length=100, label = 'User Name', widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(required=True, max_length=100, label = 'First Name', widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(required=True, max_length=100, label = 'Last Name', widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(required=True, max_length=100, label = 'Password', widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(required=True, max_length=100, label = 'Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
    phone = forms.CharField(required=True, max_length=100, label = 'Phone', widget=forms.TextInput(attrs={'class': 'form-control'}))
    organizationType = forms.CharField(required=True, max_length=100, label = 'Company', widget=forms.TextInput(attrs={'class': 'form-control'}))
    security_question = forms.CharField(label = 'Security Question', widget=forms.TextInput(attrs={'class': 'form-control'}))
    security_answer = forms.CharField(label = 'Security Answer', widget=forms.TextInput(attrs={'class': 'form-control'}))


def clean_username(self):
    user_count = amod.User.objects.filter(username=self.cleaned_data['username']).exclude(id=self.userid).count()
    if user_count >= 1:
        raise forms.ValidationError("This username is already taken.")
    return self.cleaned_data['username']



@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def create(request):
    '''Creates a new user'''
    user = amod.User()
    user.username = ''   
    user.first_name = ''
    user.last_name = ''
    user.password = ''
    user.email = ''
    user.phone = ''
    user.organizationType = ''
    user.security_question = ''
    user.security_answer = ''
    user.save()

    return HttpResponseRedirect('/account/signup.edit/{}/'.format(user.id))



@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def delete(request):
    '''Delete a user'''
    user = amod.User()
    try:
        user = amod.User.objects.get(id=request.urlparams[0])
    except amod.User.DoesNotExist:
        return HttpResponseRedirect('/mainpage/index/')

    user.delete()

    return HttpResponseRedirect('/mainpage/index/')    
