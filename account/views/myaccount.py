from django.conf import settings
from django_mako_plus.controller import view_function
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import HttpRequest
import mainpage.models as amod
from django_mako_plus.controller.router import get_renderer
from django import forms
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import authenticate, login
from django.contrib.auth import update_session_auth_hash
import random
import datetime

templater = get_renderer('account')

##Pull users from DB
@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def process_request(request):
    params = {}

    params['accounts'] = amod.User.objects.all().order_by('id')
    return templater.render_to_response(request,'myaccount.html',params)


##Edit Users
@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def edit(request):
    params = {}

    try:
        user = amod.User.objects.get(id=request.urlparams[0])
    except amod.User.DoesNotExist:
        return HttpResponseRedirect('/account/myaccount/')

    form = UserEditForm(initial={
        # 'password': user.password ,
        'last_login': user.last_login ,
        'username': user.username ,
        'first_name': user.first_name ,
        'last_name': user.last_name , 
        'email': user.email ,
        'phone': user.phone ,
        'organizationType': user.organizationType ,
        'security_question': user.security_question,
        'security_answer': user.security_answer,

        })

    if request.method == 'POST':
     form = UserEditForm(request.POST)
     form.userid = user.id
     if form.is_valid():
        # user.set_password(form.cleaned_data['password'])
        # user.last_login = form.cleaned_data['last_login']
        user.username = form.cleaned_data['username']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.email = form.cleaned_data['email']
        user.phone = form.cleaned_data['phone']
        # user.date_joined = form.cleaned_data['date_joined']
        # user.appointmentDate = form.cleaned_data['appointmentDate']
        user.security_question = form.cleaned_data['security_question']
        user.security_answer = form.cleaned_data['security_answer']
        # user.organizationType = form.cleaned_data['organizationType']
        user.save()
        return HttpResponseRedirect('/account/myaccount/${user.id}')

    params['form'] = form
    params['user'] = user

    return templater.render_to_response(request,'myaccount.edit.html',params)    

class UserEditForm(forms.Form):

    username = forms.CharField(required=True, max_length=100, label = 'User Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'User Name'}))
    first_name = forms.CharField(required=True, max_length=100, label = 'First Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}))
    last_name = forms.CharField(required=True, max_length=100, label = 'Last Name', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}))
    # password = forms.CharField(required=True, max_length=100, label = 'Password', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    email = forms.CharField(required=True, max_length=100, label = 'Email', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))
    phone = forms.CharField(required=True, max_length=100, label = 'Phone', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Phone'}))
    security_question = forms.CharField(label = 'Security Question', widget=forms.TextInput(attrs={'class': 'form-control'}))
    security_answer = forms.CharField(label = 'Security Answer', widget=forms.TextInput(attrs={'class': 'form-control'}))
    # organizationType = forms.CharField(label = 'Company', widget=forms.TextInput(attrs={'class': 'form-control'}))
    # appointmentDate = forms.DateField(label = 'Appointment Date', widget=forms.DateInput(attrs={'class': 'form-control'}))
    # date_joined = forms.DateField(label = 'Date Joined', widget=forms.DateInput(attrs={'class': 'form-control','readonly':'readonly'}))
    # last_login = forms.DateField(label = 'Last Login', widget=forms.DateInput(attrs={'class': 'form-control','readonly':'readonly'}))


def clean_username(self):
    user_count = amod.User.objects.filter(username=self.cleaned_data['username']).exclude(id=self.userid).count()
    if user_count >= 1:
        raise forms.ValidationError("This username is already taken.")
    return self.cleaned_data['username']



@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def create(request):
    '''Creates a new user'''
    user = amod.User()
    user.username = ''   
    user.first_name = ''
    user.last_name = ''
    user.password = ''
    user.email = ''
    user.phone = ''
    user.organizationType = ''
    user.security_question = ''
    user.security_answer = ''
    # user.appointmentDate = datetime.datetime.now()
    # user.last_login = datetime.datetime.now()
    # user.date_joined = datetime.datetime.now()
    user.save()

    return HttpResponseRedirect('/account/myaccount.edit/{}/'.format(user.id))



@view_function
# @permission_required('is_superuser', login_url='/account/login', raise_exception=False)
def delete(request):
    '''Delete a user'''
    user = amod.User()
    try:
        user = amod.User.objects.get(id=request.urlparams[0])
    except amod.User.DoesNotExist:
        return HttpResponseRedirect('/account/myaccount/')

    user.delete()

    return HttpResponseRedirect('/account/myaccount/')    

@view_function
def changepassword(request):

    params = {}

    current_user = request.user
    user = amod.User.objects.get(id=current_user.id)

    form = UserPasswordForm(initial={
        'password': '',
        'newpassword': '',
        'new_password': ''
        })

    if request.method == 'POST':
      form = UserPasswordForm(request.POST)
      form.userid = user.id
      form.request = request
      if form.is_valid():
        newpassword = form.cleaned_data['newpassword']
        new_password = form.cleaned_data['new_password']
        if request.user.check_password(form.cleaned_data['password']):
            if newpassword == new_password: 
               request.user.set_password(form.cleaned_data['new_password'])
               request.user.save()
               return HttpResponseRedirect('/mainpage/index/')

    params['form'] = form
    params['user'] = user
    return templater.render_to_response(request, 'myaccount.changepassword.html', params)

class UserPasswordForm(forms.Form):
 password = forms.CharField(label="Current Password", required=False, max_length=100, widget=forms.PasswordInput)
 newpassword = forms.CharField(label="New Password", required=False, min_length= 8, max_length=100, widget=forms.PasswordInput)
 new_password = forms.CharField(label="Confirm Password", required=False, min_length= 8, max_length=100, widget=forms.PasswordInput)


###########################################
###### Verify current password

 def clean_password(self):
    if self.request.user.check_password(self.cleaned_data['password']):
        return self.cleaned_data['password']
    else:
        raise forms.ValidationError("Please enter correct current password.")


#     params = {}

#     try:
#         user = amod.User.objects.get(id=request.urlparams[0])
#     except amod.User.DoesNotExist:
#         return HttpResponseRedirect('/account/myaccount/')

#     form = PasswordEditForm(initial={
#         'password': user.password ,
#         })
#     if request.method == 'POST':
#      form = PasswordEditForm(request.POST)
#      form.userid = user.id
#      if form.is_valid():
#         user.set_password(form.cleaned_data['password'])
#         user.save()
#         update_session_auth_hash(request, user)
#         request.session['user_id'] = user.id
#         return HttpResponseRedirect('/mainpage/index/')
#         # if request.method == 'POST':
#         # form = ChangePassword(request.POST)
#         # if form.is_valid():
#         #     user.set_password(form.cleaned_data['password'])
#         #     user.save()
#         #     update_session_auth_hash(request, user)
#         #     request.session['user_id'] = user.id
#         #     return HttpResponse(True)

#     params['form'] = form
#     params['user'] = user

#     return templater.render_to_response(request,'myaccount.changepassword.html',params)  


# class PasswordEditForm(forms.Form):

#     # old_password = forms.CharField(required=True, max_length=100, label = 'Old Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
#     password = forms.CharField(required=True, max_length=100, label = 'New Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
#     # new_password2 = forms.CharField(required=True, max_length=100, label = 'Confirm New Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))
    
#     # def clean(self):
#     #     password = authenticate(password=self.cleaned_data['old_password'])
#     #     if password == None:
#     #         raise forms.ValidationError('Password is incorrect.')
#     #     return self.cleaned_data