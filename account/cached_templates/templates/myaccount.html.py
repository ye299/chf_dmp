# -*- coding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1427953740.4731
_enable_loop = True
_template_filename = 'C:\\Python34\\Scripts\\CHF_DMP\\account\\templates/myaccount.html'
_template_uri = 'myaccount.html'
_source_encoding = 'ascii'
import os, os.path, re
_exports = ['header', 'content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, '/mainpage/templates/base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def header():
            return render_header(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        users = context.get('users', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'header'):
            context['self'].header(**pageargs)
        

        __M_writer('\r\n\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer("\r\n\r\n\r\n\r\n<table id='user_table' class='table table-striped table-border table-condensed'>\r\n        <tr>\r\n            <th>ID:</th>\r\n            <th>User Name:</th>\r\n            <th>First Name:</th>\r\n            <th>Last Name:</th>\r\n            <th>Email:</th>\r\n            <th>Phone:</th>\r\n            <th>Last Login:</th>\r\n            <th></th>\r\n        </tr>\r\n")
        for user in users:
            __M_writer('            <tr>\r\n                <td>')
            __M_writer(str(user.id))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(user.username))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(user.first_name))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(user.last_name))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(user.email))
            __M_writer('</td>\r\n                <td>')
            __M_writer(str(user.last_login))
            __M_writer('</td>\r\n                <td><a href="/mainpage/user.edit/')
            __M_writer(str(user.id))
            __M_writer('">Edit</a></td>\r\n            </tr>\r\n')
        __M_writer('    </table>')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_header(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def header():
            return render_header(context)
        __M_writer = context.writer()
        __M_writer('\r\n    My Account\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        user = context.get('user', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\t<div class="clearfix"></div>\r\n    <div class = "text-left">\r\n    </div>\r\n    <div>\r\n        <table id=\'user_table\'>\r\n            <tr>\r\n                <th>ID:  </th>         \r\n                <td>')
        __M_writer(str(user.id))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>User Name:  </th>\r\n                <td>')
        __M_writer(str(user.username))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>First Name:  </th>    \r\n                <td>')
        __M_writer(str(user.first_name))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Last Name:  </th>\r\n                <td>')
        __M_writer(str(user.last_name))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Email:  </th>\r\n                <td>')
        __M_writer(str(user.email))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Security Question:  </th>\r\n                <td>')
        __M_writer(str(user.security_question))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Security Answer:  </th>\r\n                <td>')
        __M_writer(str(user.security_answer))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Last Login:  </th>\r\n                <td>')
        __M_writer(str(user.last_login))
        __M_writer('</td>\r\n            </tr>\r\n            <tr>\r\n                <a class="btn btn-warning" href="/account/myaccount.edit/')
        __M_writer(str(user.id))
        __M_writer('">Edit</a>\r\n                <a class="btn btn-primary" href="/account/myaccount.changepassword/')
        __M_writer(str(user.id))
        __M_writer('">Change Password</a>\r\n            </tr>\r\n\r\n        </table>\r\n    </div>\r\n\r\n    \r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "myaccount.html", "line_map": {"27": 0, "37": 1, "42": 5, "47": 55, "48": 70, "49": 71, "50": 72, "51": 72, "52": 73, "53": 73, "54": 74, "55": 74, "56": 75, "57": 75, "58": 76, "59": 76, "60": 77, "61": 77, "62": 78, "63": 78, "64": 81, "70": 3, "76": 3, "82": 8, "89": 8, "90": 16, "91": 16, "92": 20, "93": 20, "94": 24, "95": 24, "96": 28, "97": 28, "98": 32, "99": 32, "100": 36, "101": 36, "102": 40, "103": 40, "104": 44, "105": 44, "106": 47, "107": 47, "108": 48, "109": 48, "115": 109}, "filename": "C:\\Python34\\Scripts\\CHF_DMP\\account\\templates/myaccount.html", "source_encoding": "ascii"}
__M_END_METADATA
"""
